#!/bin/bash

# possible input combinations
one_vec=("x" "y" "z" "w")
two_vec=("x y" "y x" "x z" "z x" "y z" "z y"
	       "x w" "w x" "y w" "w y" "z w" "w z")
three_vec=("x y z" "x z y" "y x z" "y z x" "z x y" "z y x"
           "x y w" "x w y" "y x w" "y w x" "w x y" "w y x"
           "x w z" "x z w" "w x z" "w z x" "z x w" "z w x"
           "w y z" "w z y" "y w z" "y z w" "z w y" "z y w")
four_vec=("x y z w" "x z y w" "y x z w" "y z x w" "z x y w" "z y x w"
          "x y w z" "x w y z" "y x w z" "y w x z" "w x y z" "w y x z"
          "x w z y" "x z w y" "w x z y" "w z x y" "z x w y" "z w x y"
          "w y z x" "w z y x" "y w z x" "y z w x" "z w y x" "z y w x")

trace_path="../pimtraces"
pimops="axpby axpbypcz axpy copy dot nrm2 scal gemv"

# axpby
for input in "${three_vec[@]}"
do
	postfix=`echo $input | sed 's/ //g'`
	file="$trace_path/axpby_${postfix}.trc"
	echo "AXPBY $input" > $file
done

# axpbypcz
for input in "${four_vec[@]}"
do
	postfix=`echo $input | sed 's/ //g'`
	file="$trace_path/axpbypcz_${postfix}.trc"
	echo "AXPBYPCZ $input" > $file
done

# axpy
for input in "${two_vec[@]}"
do
	postfix=`echo $input | sed 's/ //g'`
	file="$trace_path/axpy_${postfix}.trc"
	echo "AXPY $input" > $file
done

# copy
for input in "${two_vec[@]}"
do
	postfix=`echo $input | sed 's/ //g'`
	file="$trace_path/copy_${postfix}.trc"
	echo "COPY $input" > $file
done

# dot
for input in "${two_vec[@]}"
do
	postfix=`echo $input | sed 's/ //g'`
	file="$trace_path/dot_${postfix}.trc"
	echo "DOT $input" > $file
done

# nrm2
for input in "${one_vec[@]}"
do
	postfix=`echo $input | sed 's/ //g'`
	file="$trace_path/nrm2_${postfix}.trc"
	echo "NRM2 $input" > $file
done

# scal
for input in "${one_vec[@]}"
do
	postfix=`echo $input | sed 's/ //g'`
	file="$trace_path/scal_${postfix}.trc"
	echo "SCAL $input" > $file
done

# gemv
for input in "${two_vec[@]}"
do
	postfix=`echo $input | sed 's/ /A/g'`
	input=`echo $input | sed 's/ / A /g'`
	file="$trace_path/gemv_${postfix}.trc"
	echo "GEMV $input" > $file
done

