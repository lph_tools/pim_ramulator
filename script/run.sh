#!/bin/bash

echo_and_run() { echo "$@" ; "$@" ; }

ramulator_home=~/pim_ramulator
sim_home=$ramulator_home/sim
trc_home=$ramulator_home/pimtraces
cfg_dir=$ramulator_home/configs
bin=ramulator

cfgs="fb tbtsb obfsb"
# pimops="axpby axpbypcz axpy copy dot nrm2 scal gemv"
pimops="axpby axpbypcz axpy copy dot nrm2 scal"
mappings="block intlv"

target_cfgs=""
target_pimops=""
target_inputs=""

# check options
while getopts :c:o:i:m:elh opt
do
	case "$opt" in
		# select PIM configuration
		c) 	if [[ "$OPTARG" == "all" ]]; then
			 		target_cfgs="$cfgs"
			 	else
			 		target_cfgs="$OPTARG"
			 	fi
			 	;;

		# select operation
		o) 	if [[ "$OPTARG" == "all" ]]; then
			 		target_pimops="$pimops"
			 	else
			 		target_pimops="$OPTARG"
			 	fi
			 	;;

		# select input trace
		i) if [[ "$OPTARG" == "all" ]]; then
					all_existing_inputs="1"
			 		target_inputs=""
			 	else
			 		target_inputs="$OPTARG"
			 	fi
			 	;;
		m) if [[ "$OPTARG" == "all" ]]; then
				mapping="$mappings"
			 else
			 	mapping="$OPTARG"
			 fi
			 ;;

		# echo the commands without executing them
		e) echo_only="1";;
		l) collect_results="1";;
    h) print_usage="1";;
		*) echo "Unknown option: $opt";;
	esac
done

if [[ -z $target_cfgs || -z $target_pimops || "$print_usage" == "1" ]]; then
	# echo "[Error] Insufficient input arguments."
	echo "Usage: $0 -m [MAPPING | all] -c [CONFIGS | all] -o [PIMOPS | all] -i [TRACE_FILE | all] (-elh)"
	echo "       MAPPING = block, intlv"
	echo "       CONFIGS = fb, tbtsb, obfsb"
	echo "       PIMOPS  = axpby, axpbypcz, axpy, copy, dot, nrm2, scal, gemv"
	echo "       -e      = echo mode"
	echo "       -l      = collect results"
	echo "       -h      = help (print usage)"
	exit
elif [[ "$collect_results" !=  "1" ]]; then
	run_sim="1"
fi

# run simulation
matrix_shapes="4x16384 8x8192 16x4096 32x2048 64x1024 128x512 256x256 512x128 1024x64 2048x32 4096x16 8192x8 16384x4" 
# matrix_shapes="32x2048 64x1024 128x512 256x256 512x128 1024x64 2048x32" 

if [[ "$target_pimops" == "gemv" && "$run_sim" == "1" ]]; then
	for map in $mapping; do
		for cfg in $target_cfgs; do
			if [[ "$all_existing_inputs" == "1" ]]; then
				target_inputs=`find $trc_home -type f -name "gemv_*.trc" -print | rev | cut -d "/" -f1 | rev | cut -d "." -f1`
			fi
			input=gemv_yAx
			for gemv_opt in b2 b3; do
				for mat_shape in $matrix_shapes;do
					path="$sim_home/$map/$cfg/gemv/$input/$gemv_opt/$mat_shape"
					mkdir -p $path; cd $path
					# cp $trc_home/Axyzw_${gemv_opt}_${mat_shape}_${input##*_}.map Axyzw.map
					cp $trc_home/maps/Axyzw_${gemv_opt}_${mat_shape}.map Axyzw.map
					cfg_file="$cfg_dir/PIM_${cfg}_${map}_gemv.cfg"
					trc_file="$trc_home/$input.trc"
					cmd="$ramulator_home/$bin $cfg_file --mode=pim $trc_file"
					script_str="#!/bin/bash\n$cmd"
					echo -e $script_str > run_cmd.sh
					chmod +x run_cmd.sh
					if [[ "$echo_only" == "1" ]]; then
						echo $cmd
					else
						echo_and_run $cmd > /dev/null 2>&1 &
					fi
				done
			done
		done
	done

	wait

elif [[ "$run_sim" ==  "1" ]]; then
	for map in $mapping; do
		for cfg in $target_cfgs; do
			for pimop in $target_pimops; do
				if [[ "$all_existing_inputs" == "1" ]]; then
					target_inputs=`find $trc_home -type f -name "${pimop}_*.trc" -print | rev | cut -d "/" -f1 | rev | cut -d "." -f1`
				fi 

				for input in $target_inputs; do
					path="$sim_home/$map/$cfg/$pimop/$input"
					mkdir -p $path; cd $path
					cp $trc_home/Axyzw.map .
					cfg_file="$cfg_dir/PIM_${cfg}_${map}.cfg"
					trc_file="$trc_home/$input.trc"
					cmd="$ramulator_home/$bin $cfg_file --mode=pim $trc_file"
					script_str="#!/bin/bash\n$cmd"
					echo -e $script_str > run_cmd.sh
					chmod +x run_cmd.sh
					if [[ "$echo_only" == "1" ]]; then
						echo $cmd
					else
						echo_and_run $cmd > log&
					fi
				done
			done
		done
	done

	wait
fi


# collect results
if [[ "$target_pimops" == "gemv" && "$collect_results" ==  "1" ]]; then
	for map in $mapping; do
		for cfg in $target_cfgs; do
			if [[ "$all_existing_inputs" == "1" ]]; then
				target_inputs=`find $trc_home -type f -name "gemv_*.trc" -print | rev | cut -d "/" -f1 | rev | cut -d "." -f1`
			fi
			# for input in $target_inputs; do
				input=gemv_yAx
				for gemv_opt in b2 b3; do
					for mat_shape in $matrix_shapes;do
						path="$sim_home/$map/$cfg/gemv/$input/$gemv_opt/$mat_shape"
						cd $path
						cycle=`grep "dram_cycles" ./PIM.stats | gawk '{print $2}'`
						bytes=`grep "pim_bytes" ./PIM.stats | gawk '{print $2}'`
						# thrput=`scale=4; echo "$bytes / $cycle" | bc`
						thrput=`bc <<< "scale=4; $bytes / $cycle"`
						echo $map $cfg gemv $input $gemv_opt $mat_shape $cycle $bytes $thrput
					done
				done
		done
	done

elif [[ "$collect_results" ==  "1" ]]; then
	for map in $mapping; do
		for cfg in $target_cfgs; do
			for pimop in $target_pimops; do
				if [[ "$all_existing_inputs" == "1" ]]; then
					target_inputs=`find $trc_home -type f -name "${pimop}_*.trc" -print | rev | cut -d "/" -f1 | rev | cut -d "." -f1`
				fi 

				for input in $target_inputs; do
					path="$sim_home/$map/$cfg/$pimop/$input"
					cd $path;
					cycle=`grep "dram_cycles" ./PIM.stats | gawk '{print $2}'`
					bytes=`grep "pim_bytes" ./PIM.stats | gawk '{print $2}'`
					# thrput=`scale=4; echo "$bytes / $cycle" | bc`
					thrput=`bc <<< "scale=4; $bytes / $cycle"`	
					input_with_space=`echo $input | sed 's/_/ /'`
					echo $map $cfg $input_with_space $cycle $bytes $thrput
				done
			done
		done
	done
fi
