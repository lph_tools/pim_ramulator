#include "Allocator.h"
#include "Memory.h"
#include <cassert>
#include <cmath>

namespace ramulator {
Allocator::Allocator(const Config &cfg, const MemoryBase *memory)
    : memory_(memory) {
  assert(cfg.contains("num_local_bank"));
  assert(cfg.contains("pim_granularity"));
  assert(cfg.contains("addr_mode"));

  if (cfg["addr_mode"] == "block")
    addr_mode_ = PIM_ADDR_MODE::BLOCK;
  else
    addr_mode_ = PIM_ADDR_MODE::INTLV;

  num_local_bank_ = std::stoi(cfg["num_local_bank"]);
  tot_num_pe_ = memory_->num_total_bank() / std::stoi(cfg["pim_granularity"]);
  row_sz_ = memory_->row_size(); // in bytes

  addr_t base = 0;
  for (uint32_t i = 0; i < num_local_bank_; ++i) {
    base_addr_.push_back(base);
    // base = memory_->calc_addr(base, row_sz_);
    base = memory_->add_to_bank_addr(base, 1);
  }
}

void Allocator::allocate_vector(addr_t base_bank,
                                std::vector<addr_t> &base_addr_vec,
                                size_t size) {
  if (addr_mode_ == PIM_ADDR_MODE::BLOCK)
    base_addr_vec.push_back(base_addr_[base_bank]);
  else
    for (auto &addr : base_addr_)
      base_addr_vec.push_back(addr);

  update(base_bank, size);
}

addr_t Allocator::allocate(addr_t base_bank, size_t size) {
  addr_t base_addr = base_addr_[base_bank];
  update(base_bank, size);

  return base_addr;
}

void Allocator::update(addr_t base_bank, size_t size) {
  assert(num_local_bank_ >= base_bank);
  // size_t size_pe = size / tot_num_pe_; // size per PE

  if (addr_mode_ == PIM_ADDR_MODE::INTLV) {
    double alloc_num_row =
        std::ceil((double)size / (num_local_bank_ * row_sz_));

    for_each(base_addr_.begin(), base_addr_.end(),
             [alloc_num_row, this](addr_t &base) {
               base = this->memory_->add_to_row_addr(base, alloc_num_row);
             });

  } else { // block mode
    base_addr_[base_bank] += size;
  }
}

uint32_t Allocator::num_local_bank() { return num_local_bank_; }
uint32_t Allocator::tot_num_pe() { return tot_num_pe_; }

} // namespace ramulator