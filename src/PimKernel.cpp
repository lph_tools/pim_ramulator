#include "PimKernel.h"

namespace ramulator {

bool PimKernel::get_next_packet(PimPacket &packet) {
  std::string line;
  std::getline(trace_file_, line);
  if (trace_file_.eof())
    return true;

  std::istringstream iss(line);
  uint32_t num_words = this->num_words(line);
  assert(num_words >= 1);
  std::string op_str;
  iss >> op_str;
  uint32_t mode = 0;
  std::string x, y, z, w, A;
  uint32_t num_local_bank = alloc_.num_local_bank();

  if (addr_mode_ == PIM_ADDR_MODE::INTLV)
    mode |= 0x10;

  switch (PimOp::op_type[op_str]) {
  case PimOp::OPTYPE::AXPBY: {
    assert(num_words == 4);
    iss >> z;
    iss >> x;
    iss >> y;

    assert(var_table_[x].num_row == var_table_[y].num_row);
    assert(var_table_[x].num_row == var_table_[z].num_row);

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::AXPBY),
                var_table_[x].base_addr_vec[0],
                var_table_[y].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[z].base_addr_vec[0],
                var_table_[z].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    } else {
      assert(var_table_[x].base_addr_vec.size() == num_local_bank);
      assert(var_table_[y].base_addr_vec.size() == num_local_bank);
      assert(var_table_[z].base_addr_vec.size() == num_local_bank);

      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      uint32_t z_base_bank = var_table_[z].base_bank;

      mode |= (x_base_bank << 12);
      mode |= (y_base_bank << 16);
      mode |= (z_base_bank << 20);
      packet = {mode,
                uint32_t(PimOp::OPTYPE::AXPBY),
                var_table_[x].base_addr_vec[x_base_bank],
                var_table_[y].base_addr_vec[y_base_bank],
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[z].base_addr_vec[z_base_bank],
                var_table_[z].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    }
    break;

    // assert(num_words == 4);

    // iss >> z;
    // iss >> x;
    // iss >> y;

    // assert(var_table_[x].num_row == var_table_[y].num_row);
    // assert(var_table_[x].num_row == var_table_[z].num_row);

    // packet = {mode,
    //           uint32_t(PimOp::OPTYPE::AXPBY),
    //           var_table_[x].base_addr,
    //           var_table_[y].base_addr,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           var_table_[z].base_addr,
    //           var_table_[z].num_row,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF};

    // break;
  }
  case PimOp::OPTYPE::AXPBYPCZ: {
    assert(num_words == 5);
    iss >> w;
    iss >> x;
    iss >> y;
    iss >> z;

    assert(var_table_[x].num_row == var_table_[y].num_row);
    assert(var_table_[x].num_row == var_table_[z].num_row);
    assert(var_table_[x].num_row == var_table_[w].num_row);

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::AXPBYPCZ),
                var_table_[x].base_addr_vec[0],
                var_table_[y].base_addr_vec[0],
                var_table_[z].base_addr_vec[0],
                0xFFFFFFFF,
                var_table_[w].base_addr_vec[0],
                var_table_[w].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};

    } else {
      assert(var_table_[x].base_addr_vec.size() == num_local_bank);
      assert(var_table_[y].base_addr_vec.size() == num_local_bank);
      assert(var_table_[z].base_addr_vec.size() == num_local_bank);
      assert(var_table_[w].base_addr_vec.size() == num_local_bank);

      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      uint32_t z_base_bank = var_table_[z].base_bank;
      uint32_t w_base_bank = var_table_[w].base_bank;

      mode |= (x_base_bank << 12);
      mode |= (y_base_bank << 16);
      mode |= (z_base_bank << 20);
      mode |= (w_base_bank << 24);
      packet = {mode,
                uint32_t(PimOp::OPTYPE::AXPBYPCZ),
                var_table_[x].base_addr_vec[x_base_bank],
                var_table_[y].base_addr_vec[y_base_bank],
                var_table_[z].base_addr_vec[z_base_bank],
                0xFFFFFFFF,
                var_table_[w].base_addr_vec[w_base_bank],
                var_table_[w].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    }
    break;

    // assert(num_words == 5);

    // iss >> w;
    // iss >> x;
    // iss >> y;
    // iss >> z;

    // assert(var_table_[x].num_row == var_table_[y].num_row);
    // assert(var_table_[x].num_row == var_table_[z].num_row);
    // assert(var_table_[x].num_row == var_table_[w].num_row);

    // packet = {mode,
    //           uint32_t(PimOp::OPTYPE::AXPBYPCZ),
    //           var_table_[x].base_addr,
    //           var_table_[y].base_addr,
    //           var_table_[z].base_addr,
    //           0xFFFFFFFF,
    //           var_table_[w].base_addr,
    //           var_table_[w].num_row,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF};

    // break;
  }
  case PimOp::OPTYPE::AXPY: {
    assert(num_words == 3);
    iss >> y;
    iss >> x;

    assert(var_table_[x].num_row == var_table_[y].num_row);

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::AXPY),
                var_table_[x].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[y].base_addr_vec[0],
                var_table_[y].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};

    } else {
      assert(var_table_[x].base_addr_vec.size() == num_local_bank);
      assert(var_table_[y].base_addr_vec.size() == num_local_bank);

      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      uint32_t bank_distance = std::abs(x_base_bank - y_base_bank);

      if (bank_distance == 0) {
        assert(0); // TODO: change the OpCopy first

      } else if (bank_distance % 2 == 0) {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::AXPY),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 1) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 1) % num_local_bank],
            0xFFFFFFFF,
            var_table_[y].base_addr_vec[y_base_bank],
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};

      } else {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::AXPY),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 2) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 2) % num_local_bank],
            0xFFFFFFFF,
            var_table_[y].base_addr_vec[y_base_bank],
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};
      }
    }
    break;

    // assert(num_words == 3);

    // iss >> y;
    // iss >> x;

    // assert(var_table_[x].num_row == var_table_[y].num_row);

    // packet = {mode,
    //           uint32_t(PimOp::OPTYPE::AXPBY),
    //           var_table_[x].base_addr,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           var_table_[y].base_addr,
    //           var_table_[y].num_row,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF};
    // break;
  }
  case PimOp::OPTYPE::COPY: {
    assert(num_words == 3);
    iss >> y;
    iss >> x;

    assert(var_table_[x].num_row == var_table_[y].num_row);

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::COPY),
                var_table_[x].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[y].base_addr_vec[0],
                var_table_[y].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};

    } else {
      assert(var_table_[x].base_addr_vec.size() == num_local_bank);
      assert(var_table_[y].base_addr_vec.size() == num_local_bank);

      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      uint32_t bank_distance = std::abs(x_base_bank - y_base_bank);

      if (bank_distance == 0) {
        assert(0); // TODO: change the OpCopy first

      } else if (bank_distance % 2 == 0) {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::COPY),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 1) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 1) % num_local_bank],
            0xFFFFFFFF,
            var_table_[y].base_addr_vec[y_base_bank],
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};

      } else {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::COPY),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 2) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 2) % num_local_bank],
            0xFFFFFFFF,
            var_table_[y].base_addr_vec[y_base_bank],
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};
      }
    }
    break;
  }

  case PimOp::OPTYPE::DOT: {
    assert(num_words == 3);
    iss >> x;
    iss >> y;

    assert(var_table_[x].num_row == var_table_[y].num_row);

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::DOT),
                var_table_[x].base_addr_vec[0],
                var_table_[y].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[y].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    } else {
      assert(var_table_[x].base_addr_vec.size() == num_local_bank);
      assert(var_table_[y].base_addr_vec.size() == num_local_bank);

      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      uint32_t bank_distance = std::abs(x_base_bank - y_base_bank);

      if (bank_distance == 0) {
        assert(0); // TODO: change the OpCopy first

      } else if (bank_distance % 2 == 0) {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::DOT),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 1) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 1) % num_local_bank],
            var_table_[y].base_addr_vec[y_base_bank],
            0xFFFFFFFF,
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};

      } else {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::DOT),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 2) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 2) % num_local_bank],
            var_table_[y].base_addr_vec[y_base_bank],
            0xFFFFFFFF,
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};
      }
    }
    break;

    // assert(num_words == 3);
    // iss >> y;
    // iss >> x;

    // assert(var_table_[x].num_row == var_table_[y].num_row);

    // packet = {mode,
    //           uint32_t(PimOp::OPTYPE::DOT),
    //           var_table_[x].base_addr,
    //           var_table_[y].base_addr,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           var_table_[y].num_row,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF};
    // break;
  }
  case PimOp::OPTYPE::DOTC: {
    assert(num_words == 3);
    iss >> x;
    iss >> y;

    assert(var_table_[x].num_row == var_table_[y].num_row);

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::DOTC),
                var_table_[x].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[y].base_addr_vec[0],
                var_table_[y].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};

    } else {
      assert(var_table_[x].base_addr_vec.size() == num_local_bank);
      assert(var_table_[y].base_addr_vec.size() == num_local_bank);

      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      uint32_t bank_distance = std::abs(x_base_bank - y_base_bank);

      if (bank_distance == 0) {
        assert(0); // TODO: change the OpCopy first

      } else if (bank_distance % 2 == 0) {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::DOTC),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 1) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 1) % num_local_bank],
            0xFFFFFFFF,
            var_table_[y].base_addr_vec[y_base_bank],
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};

      } else {
        mode |= (x_base_bank << 12);
        mode |= (y_base_bank << 16);
        packet = {
            mode,
            uint32_t(PimOp::OPTYPE::DOTC),
            var_table_[x].base_addr_vec[x_base_bank],
            var_table_[x].base_addr_vec[(x_base_bank + 2) % num_local_bank],
            var_table_[y].base_addr_vec[(y_base_bank + 2) % num_local_bank],
            0xFFFFFFFF,
            var_table_[y].base_addr_vec[y_base_bank],
            var_table_[y].num_row,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF};
      }
    }
    break;

    // assert(num_words == 3);
    // iss >> y;
    // iss >> x;

    // assert(var_table_[x].num_row == var_table_[y].num_row);

    // packet = {mode,
    //           uint32_t(PimOp::OPTYPE::DOTC),
    //           var_table_[x].base_addr,
    //           var_table_[y].base_addr,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           var_table_[y].num_row,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF};
    // break;
  }
  case PimOp::OPTYPE::GS: {
    assert(1);
    break;
  case PimOp::OPTYPE::JACOBI:
    assert(1);
    break;
  case PimOp::OPTYPE::NRM2:
    assert(num_words == 2);
    iss >> x;

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::NRM2),
                var_table_[x].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[x].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    } else {

      uint32_t x_base_bank = var_table_[x].base_bank;
      mode |= (x_base_bank << 12);

      packet = {mode,
                uint32_t(PimOp::OPTYPE::NRM2),
                var_table_[x].base_addr_vec[x_base_bank],
                var_table_[x].base_addr_vec[(x_base_bank + 1) % num_local_bank],
                var_table_[x].base_addr_vec[(x_base_bank + 2) % num_local_bank],
                var_table_[x].base_addr_vec[(x_base_bank + 3) % num_local_bank],
                0xFFFFFFFF,
                var_table_[x].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    }
    break;

    // assert(num_words == 2);
    // iss >> x;

    // packet = {mode,
    //           uint32_t(PimOp::OPTYPE::NRM2),
    //           var_table_[x].base_addr,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           var_table_[x].num_row,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF};
    // break;
  }
  case PimOp::OPTYPE::SCAL: {
    assert(num_words == 2);
    iss >> x;

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::SCAL),
                var_table_[x].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[x].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};

    } else {
      uint32_t x_base_bank = var_table_[x].base_bank;
      mode |= (x_base_bank << 12);

      packet = {mode,
                uint32_t(PimOp::OPTYPE::SCAL),
                var_table_[x].base_addr_vec[x_base_bank],
                var_table_[x].base_addr_vec[(x_base_bank + 1) % num_local_bank],
                var_table_[x].base_addr_vec[(x_base_bank + 2) % num_local_bank],
                var_table_[x].base_addr_vec[(x_base_bank + 3) % num_local_bank],
                0xFFFFFFFF,
                var_table_[x].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    }
    break;

    // assert(num_words == 2);
    // iss >> x;

    // packet = {mode,
    //           uint32_t(PimOp::OPTYPE::SCAL),
    //           var_table_[x].base_addr,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           var_table_[x].num_row,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF,
    //           0xFFFFFFFF};
    // break;
  }
  case PimOp::OPTYPE::GEMV: {
    assert(num_words == 4);

    iss >> y;
    iss >> A;
    iss >> x;

    assert(var_table_[A].num_col == var_table_[x].num_row);
    assert(var_table_[A].num_row == var_table_[y].num_row);

    addr_t op3;
    if (var_table_[A].base_addr_vec.size() == 2) {
      op3 = 0xFFFFFFFF;
    } else if (var_table_[A].base_addr_vec.size() == 3) {
      mode |= 0x100;
      op3 = var_table_[A].base_addr_vec[2];
    } else {
      std::cerr << "[Error] Only bank2 and bank3 mode is supported for GEMV."
                << std::endl;
      exit(EXIT_FAILURE);
    }

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::GEMV),
                var_table_[A].base_addr_vec[0],
                var_table_[A].base_addr_vec[1],
                op3,
                var_table_[x].base_addr_vec[0],
                var_table_[y].base_addr_vec[0],
                var_table_[A].num_row,
                var_table_[A].num_col,
                0xFFFFFFFF,
                0xFFFFFFFF};
    } else {
      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      packet = {mode,
                uint32_t(PimOp::OPTYPE::GEMV),
                var_table_[A].base_addr_vec[0],
                var_table_[A].base_addr_vec[1],
                op3,
                var_table_[x].base_addr_vec[x_base_bank],
                var_table_[y].base_addr_vec[y_base_bank],
                var_table_[A].num_row,
                var_table_[A].num_col,
                0xFFFFFFFF,
                0xFFFFFFFF};
    }

    break;
  }
  case PimOp::OPTYPE::XMY: {
    assert(num_words == 4);
    iss >> z;
    iss >> x;
    iss >> y;

    assert(var_table_[x].num_row == var_table_[y].num_row);
    assert(var_table_[x].num_row == var_table_[z].num_row);

    if (addr_mode_ == PIM_ADDR_MODE::BLOCK) {
      packet = {mode,
                uint32_t(PimOp::OPTYPE::XMY),
                var_table_[x].base_addr_vec[0],
                var_table_[y].base_addr_vec[0],
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[z].base_addr_vec[0],
                var_table_[z].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    } else {
      assert(var_table_[x].base_addr_vec.size() == num_local_bank);
      assert(var_table_[y].base_addr_vec.size() == num_local_bank);
      assert(var_table_[z].base_addr_vec.size() == num_local_bank);

      uint32_t x_base_bank = var_table_[x].base_bank;
      uint32_t y_base_bank = var_table_[y].base_bank;
      uint32_t z_base_bank = var_table_[z].base_bank;

      mode |= (x_base_bank << 12);
      mode |= (y_base_bank << 16);
      mode |= (z_base_bank << 20);
      packet = {mode,
                uint32_t(PimOp::OPTYPE::XMY),
                var_table_[x].base_addr_vec[x_base_bank],
                var_table_[y].base_addr_vec[y_base_bank],
                0xFFFFFFFF,
                0xFFFFFFFF,
                var_table_[z].base_addr_vec[z_base_bank],
                var_table_[z].num_row,
                0xFFFFFFFF,
                0xFFFFFFFF,
                0xFFFFFFFF};
    }
    break;
  }

  default:
    std::cerr << "[Error] Undefined PimOp " << op_str << " at line "
              << line_number_ << " in " << trace_file_name_ << std::endl;
    exit(EXIT_FAILURE);
    break;
  }

  ++line_number_;

  return false;
}

} // namespace ramulator