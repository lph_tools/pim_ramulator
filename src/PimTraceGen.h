#pragma once

#include "Config.h"
#include "PimKernel.h"
#include "PimPacket.h"
#include "Processor.h"

#include <iostream>
#include <memory>

namespace ramulator {

class MemoryBase;

class PimTraceGen {
public:
  PimTraceGen() {}
  PimTraceGen(const char *pim_tracename);
  // PimTraceGen(PIM_ADDR_MODE addr_mode, const std::string &map_file,
  // const std::string &trace_file);
  PimTraceGen(const Config &cfg, const char *pim_tracename, MemoryBase *memory);

  bool read_pim_packet(PimPacket &packet);

private:
  Trace trace_;
  enum class MODE : uint32_t { FILE_READ, REAL_TIME, SIZE } mode_;
  PimKernel kernel_;

  bool read_from_file(PimPacket &packet);
  bool generate_packet(PimPacket &packet);

  std::function<bool(PimPacket &)> gen_func_[uint32_t(MODE::SIZE)] = {
      // Read PIM traces from file
      [this](PimPacket &packet) { return this->read_from_file(packet); },

      // Generate PIM packets
      [this](PimPacket &packet) { return this->generate_packet(packet); }};
};

inline bool PimTraceGen::read_pim_packet(PimPacket &packet) {
  return gen_func_[uint32_t(mode_)](packet);
}

} // namespace ramulator