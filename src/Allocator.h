#pragma once

#include "Config.h"
#include "common.h"

namespace ramulator {

class MemoryBase;

class Allocator {
public:
  Allocator() {}
  Allocator(const Config &cfg, const MemoryBase *memory);
  void allocate_vector(addr_t base_bank, std::vector<addr_t> &base_addr_vec,
                       size_t size);
  addr_t allocate(addr_t base_bank, size_t size);
  uint32_t num_local_bank();
  uint32_t tot_num_pe();

private:
  void update(addr_t base_bank, size_t size);
  std::vector<addr_t> base_addr_;
  uint32_t tot_num_pe_;
  uint32_t num_local_bank_;
  PIM_ADDR_MODE addr_mode_;
  uint32_t row_sz_;
  const MemoryBase *memory_;
};

} // namespace ramulator