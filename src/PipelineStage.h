#pragma once
#include "common.h"
#include <vector>

namespace ramulator {

class PimInstruction;

class PipelineStage {
public:
  PipelineStage(STAGE stage, STAGE prev_stage, PipelineStage *next,
                uint32_t num_slots);
  ~PipelineStage();

  bool tick();
  void reset();
  bool add_instruction(PimInstruction *p);
  void set_next(PipelineStage *next);

private:
  STAGE stage_;
  STAGE prev_stage_;
  PipelineStage *next_;
  std::vector<PimInstruction *> slot_;
  uint32_t slot_capacity_;
};

} // namespace ramulator