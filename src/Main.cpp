#include "Config.h"
#include "Controller.h"
#include "DRAM.h"
#include "HostAccessGen.h"
#include "Memory.h"
#include "PimController.h"
#include "PimMemController.h"
#include "PimTraceGen.h"
#include "Processor.h"
#include "RandomAccess.h"
#include "SpeedyController.h"
#include "Statistics.h"
#include "StreamAccess.h"
#include "StreamRandomAccess.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <map>
#include <stdlib.h>

/* Standards */
#include "ALDRAM.h"
#include "DDR3.h"
#include "DDR4.h"
#include "DSARP.h"
#include "GDDR5.h"
#include "Gem5Wrapper.h"
#include "HBM.h"
#include "LPDDR3.h"
#include "LPDDR4.h"
#include "PIM.h"
#include "SALP.h"
#include "TLDRAM.h"
#include "WideIO.h"
#include "WideIO2.h"

using namespace std;
using namespace ramulator;

bool ramulator::warmup_complete = false;

long ramulator::lrand(void) {
  if (sizeof(int) < sizeof(long)) {
    return static_cast<long>(std::rand()) << (sizeof(int) * 8) | std::rand();
  }

  return std::rand();
}

template <typename T>
void run_dramtrace(const Config &configs, Memory<T, Controller> &memory,
                   const char *tracename) {
  /* initialize DRAM trace */
  Trace trace(tracename);

  /* run simulation */
  bool stall = false, end = false;
  int reads = 0, writes = 0, clks = 0;
  long addr = 0;
  Request::Type type = Request::Type::READ;
  map<int, int> latencies;
  auto read_complete = [&latencies](Request &r) {
    latencies[r.depart - r.arrive]++;
  };

  Request req(addr, type, read_complete);

  while (!end || memory.pending_requests()) {
    if (!end && !stall) {
      end = !trace.get_dramtrace_request(addr, type);
    }

    if (!end) {
      req.addr = addr;
      req.type = type;
      stall = !memory.send(req);
      if (!stall) {
        if (type == Request::Type::READ)
          reads++;
        else if (type == Request::Type::WRITE)
          writes++;
      }
    } else {
      memory.set_high_writeq_watermark(
          0.0f); // make sure that all write requests in the
                 // write queue are drained
    }

    memory.tick();
    clks++;
    Stats::curTick++; // memory clock, global, for Statistics
  }
  // This a workaround for statistics set only initially lost in the end
  memory.finish();
  Stats::statlist.printall();
}

// bool read_pim_packet(PimPacket &packet, Trace &trace) {
//   bool last = false;
//   const uint32_t packet_sz = 11;

//   for (uint32_t i = 0; i < packet_sz; i++) {
//     PimPacket::Type type;
//     long content;

//     last = !trace.get_pimtrace_request(content, type);

//     if (last) {
//       if (i != 0 && type != PimPacket::Type::PACKET_TAIL)
//         std::cout << "[Error] Incomplete PIM packet" << std::endl;
//       return true;
//     }

//     // Decode PIM command
//     if (i == 0) {
//       assert(type == PimPacket::Type::PACKET_HEAD);
//       packet.mode = content;

//     } else if (i == 1) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.op = content;

//     } else if (i == 2) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.base1 = content;

//     } else if (i == 3) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.base2 = content;

//     } else if (i == 4) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.base3 = content;

//     } else if (i == 5) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.base4 = content;

//     } else if (i == 6) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.baser = content;

//     } else if (i == 7) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.size = content;

//     } else if (i == 8) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.arg1 = content;

//     } else if (i == 9) {
//       assert(type == PimPacket::Type::PACKET_BODY);
//       packet.arg2 = content;

//     } else if (i == 10) {
//       assert(type == PimPacket::Type::PACKET_TAIL);
//       packet.arg3 = content;
//     }
//   }

//   return last;
// }

void print_packet(PimPacket &p) {
  std::cout << "\n-----[Packet info]-----" << std::endl;
  std::cout << "mode: 0x" << std::hex << p.mode << std::endl;
  std::cout << "op: 0x" << p.op << std::endl;
  std::cout << "base1: 0x" << p.base1 << std::endl;
  std::cout << "base2: 0x" << p.base2 << std::endl;
  std::cout << "base3: 0x" << p.base3 << std::endl;
  std::cout << "base4: 0x" << p.base4 << std::endl;
  std::cout << "baser: 0x" << p.baser << std::endl;
  std::cout << "size: 0x" << p.size << std::endl;
  std::cout << "arg1: 0x" << p.arg1 << std::endl;
  std::cout << "arg2: 0x" << p.arg2 << std::endl;
  std::cout << "arg3: 0x" << p.arg3 << std::dec << std::endl;
}

template <typename T>
void run_pimtrace(const Config &configs, Memory<T, Controller> &memory,
                  const char *pim_tracename) {
  /* initialize PIM trace */
  std::unique_ptr<PimTraceGen> pimtrace_gen;

  if (configs.contains("pimtrace_mode")) {
    if (configs["pimtrace_mode"].compare("real_time") == 0)
      pimtrace_gen =
          std::make_unique<PimTraceGen>(configs, pim_tracename, &memory);
    else
      pimtrace_gen = std::make_unique<PimTraceGen>(pim_tracename);
  } else {
    // default
    pimtrace_gen = std::make_unique<PimTraceGen>(pim_tracename);
  }

  bool trace_end = false, end = false, stall = false;
  Request::Type type = Request::Type::READ;
  long addr = 0;
  Request req(addr, type);

  PimController pim_ctrl(configs, &memory);

  HostAccessGen host_gen;
  if (configs.contains("host_access_pattern")) {
    if (configs["host_access_pattern"].compare("random") == 0) {
      assert(configs.contains("random_count"));
      host_gen = RandomAccess(std::stoi(configs["random_count"]));

    } else if (configs["host_access_pattern"].compare("stream") == 0) {
      assert(configs.contains("stream_start"));
      assert(configs.contains("stream_end"));
      assert(configs.contains("stream_stride"));

      addr_t start = std::stol(configs["stream_start"]);
      addr_t end = std::stol(configs["stream_end"]);
      addr_t stride = std::stol(configs["stream_stride"]);

      host_gen = StreamAccess(start, end, stride);

    } else if (configs["host_access_pattern"].compare("stream_random") == 0) {
      assert(configs.contains("random_count"));
      assert(configs.contains("stream_count"));
      assert(configs.contains("stream_stride"));

      addr_t num_random = std::stol(configs["random_count"]);
      addr_t num_stream = std::stol(configs["stream_count"]);
      addr_t stride = std::stol(configs["stream_stride"]);

      host_gen = StreamRandomAccess(num_random, num_stream, stride);

    } else {
      std::cout << "[Error] Undefined host access pattern." << std::endl;
      exit(EXIT_FAILURE);
    }

  } else {
    host_gen = StreamAccess(1, 0, 0); // No host access generation
  }

  while (!trace_end || memory.pending_requests() || pim_ctrl.busy() ||
         !host_gen.no_more_addr() || !end) {
    if (pim_ctrl.ready() && !trace_end) {
      PimPacket packet;
      trace_end = pimtrace_gen->read_pim_packet(packet);
      print_packet(packet);
      // if (trace_end)
      //   continue;
      if (!trace_end)
        pim_ctrl.launch(packet);
    }

    if (!end && !stall) {
      end = host_gen.no_more_addr();
      addr = host_gen.next_addr();
    }

    if (!end) {
      req.addr = addr;
      req.type = type;
      stall = !memory.send(req);
    }

    pim_ctrl.tick();
    memory.tick();
    Stats::curTick++; // memory clock, global, for Statistics
  }

  // This a workaround for statistics set only initially lost in the end
  memory.finish();
  Stats::statlist.printall();
}

template <typename T>
void run_cputrace(const Config &configs, Memory<T, Controller> &memory,
                  const std::vector<const char *> &files) {
  int cpu_tick = configs.get_cpu_tick();
  int mem_tick = configs.get_mem_tick();
  auto send = bind(&Memory<T, Controller>::send, &memory, placeholders::_1);
  Processor proc(configs, files, send, memory);

  long warmup_insts = configs.get_warmup_insts();
  bool is_warming_up = (warmup_insts != 0);

  for (long i = 0; is_warming_up; i++) {
    proc.tick();
    Stats::curTick++;
    if (i % cpu_tick == (cpu_tick - 1))
      for (int j = 0; j < mem_tick; j++)
        memory.tick();

    is_warming_up = false;
    for (int c = 0; c < proc.cores.size(); c++) {
      if (proc.cores[c]->get_insts() < warmup_insts)
        is_warming_up = true;
    }
  }

  warmup_complete = true;
  printf("Warmup complete! Resetting stats...\n");
  Stats::reset_stats();
  proc.reset_stats();
  assert(proc.get_insts() == 0);

  printf("Starting the simulation...\n");

  int tick_mult = cpu_tick * mem_tick;
  for (long i = 0;; i++) {
    if (((i % tick_mult) % mem_tick) ==
        0) { // When the CPU is ticked cpu_tick times,
             // the memory controller should be ticked mem_tick times
      proc.tick();
      Stats::curTick++; // processor clock, global, for Statistics

      if (configs.calc_weighted_speedup()) {
        if (proc.has_reached_limit()) {
          break;
        }
      } else {
        if (configs.is_early_exit()) {
          if (proc.finished())
            break;
        } else {
          if (proc.finished() && (memory.pending_requests() == 0))
            break;
        }
      }
    }

    if (((i % tick_mult) % cpu_tick) ==
        0) // TODO_hasan: Better if the processor ticks the memory controller
      memory.tick();
  }
  // This a workaround for statistics set only initially lost in the end
  memory.finish();
  Stats::statlist.printall();
}

template <typename T>
void start_run(const Config &configs, T *spec,
               const vector<const char *> &files) {
  // initiate controller and memory
  int C = configs.get_channels(), R = configs.get_ranks();
  // Check and Set channel, rank number
  spec->set_channel_number(C);
  spec->set_rank_number(R);
  std::vector<Controller<T> *> ctrls;
  for (int c = 0; c < C; c++) {
    DRAM<T> *channel = new DRAM<T>(spec, T::Level::Channel);
    channel->id = c;
    channel->regStats("");
    Controller<T> *ctrl = new Controller<T>(configs, channel);
    ctrls.push_back(ctrl);
  }
  Memory<T, Controller> memory(configs, ctrls);

  assert(files.size() != 0);
  if (configs["trace_type"] == "CPU") {
    run_cputrace(configs, memory, files);
  } else if (configs["trace_type"] == "DRAM") {
    run_dramtrace(configs, memory, files[0]);
  } else if (configs["trace_type"] == "PIM") {
    run_pimtrace(configs, memory, files[0]);
  }
}

int main(int argc, const char *argv[]) {
  if (argc < 2) {
    printf("Usage: %s <configs-file> --mode=cpu,dram,pim [--stats <filename>] "
           "<trace-filename1> <trace-filename2>\n"
           "Example: %s ramulator-configs.cfg --mode=cpu cpu.trace cpu.trace\n",
           argv[0], argv[0]);
    return 0;
  }

  Config configs(argv[1]);

  const std::string &standard = configs["standard"];
  assert(standard != "" || "DRAM standard should be specified.");

  const char *trace_type = strstr(argv[2], "=");
  trace_type++;
  if (strcmp(trace_type, "cpu") == 0) {
    configs.add("trace_type", "CPU");
  } else if (strcmp(trace_type, "dram") == 0) {
    configs.add("trace_type", "DRAM");
  } else if (strcmp(trace_type, "pim") == 0) {
    configs.add("trace_type", "PIM");
  } else {
    printf("invalid trace type: %s\n", trace_type);
    assert(false);
  }

  int trace_start = 3;
  string stats_out;
  if (strcmp(argv[3], "--stats") == 0) {
    Stats::statlist.output(argv[4]);
    stats_out = argv[4];
    trace_start = 5;
  } else {
    Stats::statlist.output(standard + ".stats");
    stats_out = standard + string(".stats");
  }
  std::vector<const char *> files(&argv[trace_start], &argv[argc]);
  configs.set_core_num(argc - trace_start);

  if (standard == "DDR3") {
    DDR3 *ddr3 = new DDR3(configs["org"], configs["speed"]);
    start_run(configs, ddr3, files);
  } else if (standard == "DDR4") {
    DDR4 *ddr4 = new DDR4(configs["org"], configs["speed"]);
    start_run(configs, ddr4, files);
  } else if (standard == "SALP-MASA") {
    SALP *salp8 = new SALP(configs["org"], configs["speed"], "SALP-MASA",
                           configs.get_subarrays());
    start_run(configs, salp8, files);
  } else if (standard == "LPDDR3") {
    LPDDR3 *lpddr3 = new LPDDR3(configs["org"], configs["speed"]);
    start_run(configs, lpddr3, files);
  } else if (standard == "LPDDR4") {
    // total cap: 2GB, 1/2 of others
    LPDDR4 *lpddr4 = new LPDDR4(configs["org"], configs["speed"]);
    start_run(configs, lpddr4, files);
  } else if (standard == "GDDR5") {
    GDDR5 *gddr5 = new GDDR5(configs["org"], configs["speed"]);
    start_run(configs, gddr5, files);
  } else if (standard == "HBM") {
    HBM *hbm = new HBM(configs["org"], configs["speed"]);
    start_run(configs, hbm, files);
  } else if (standard == "WideIO") {
    // total cap: 1GB, 1/4 of others
    WideIO *wio = new WideIO(configs["org"], configs["speed"]);
    start_run(configs, wio, files);
  } else if (standard == "WideIO2") {
    // total cap: 2GB, 1/2 of others
    WideIO2 *wio2 =
        new WideIO2(configs["org"], configs["speed"], configs.get_channels());
    wio2->channel_width *= 2;
    start_run(configs, wio2, files);
  }
  // Various refresh mechanisms
  else if (standard == "DSARP") {
    DSARP *dsddr3_dsarp =
        new DSARP(configs["org"], configs["speed"], DSARP::Type::DSARP,
                  configs.get_subarrays());
    start_run(configs, dsddr3_dsarp, files);
  } else if (standard == "ALDRAM") {
    ALDRAM *aldram = new ALDRAM(configs["org"], configs["speed"]);
    start_run(configs, aldram, files);
  } else if (standard == "TLDRAM") {
    TLDRAM *tldram =
        new TLDRAM(configs["org"], configs["speed"], configs.get_subarrays());
    start_run(configs, tldram, files);
  } else if (standard == "PIM") {
    PIM *pim = new PIM(configs["org"], configs["speed"],
                       std::stoi(configs["pim_granularity"]));
    start_run(configs, pim, files);
  }

  printf("Simulation done. Statistics written to %s\n", stats_out.c_str());

  return 0;
}
