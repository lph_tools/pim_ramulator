#include "PimOp.h"

std::map<std::string, enum PimOp::OPTYPE> PimOp::op_type = {
    {"AXPBY", PimOp::OPTYPE::AXPBY}, {"AXPBYPCZ", PimOp::OPTYPE::AXPBYPCZ},
    {"AXPY", PimOp::OPTYPE::AXPY},   {"COPY", PimOp::OPTYPE::COPY},
    {"DOT", PimOp::OPTYPE::DOT},     {"DOTC", PimOp::OPTYPE::DOTC},
    {"GS", PimOp::OPTYPE::GS},       {"JACOBI", PimOp::OPTYPE::JACOBI},
    {"NRM2", PimOp::OPTYPE::NRM2},   {"SCAL", PimOp::OPTYPE::SCAL},
    {"GEMV", PimOp::OPTYPE::GEMV},   {"XMY", PimOp::OPTYPE::XMY},
    {"COMM", PimOp::OPTYPE::COMM}};
