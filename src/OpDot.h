#pragma once

#include "Memory.h"
#include "OpBase.h"
#include "common.h"

namespace ramulator {

class OpBase;
class ProcessingElement;

class OpDot : public OpBase {
public:
  OpDot(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
        PimOpType type = PimOpType::NORMAL);
  virtual std::function<bool(uint32_t)> *done_func() { return done_func_; }
  virtual std::function<bool(uint32_t)> *exec_func() { return exec_func_; }
  virtual uint32_t last_index() { return last_atom_index_; }

private:
  void init_done();
  void init_exec();

  std::function<bool(uint32_t)> done_func_[uint32_t(STAGE::SIZE)];
  std::function<bool(uint32_t)> exec_func_[uint32_t(STAGE::SIZE)];
  uint32_t last_atom_index_;
};

} // namespace ramulator