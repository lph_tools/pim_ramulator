#pragma once
#include <cstdlib>
#include <iostream>

namespace ramulator {

enum class PimOpType : uint32_t { NORMAL, DONE, SIZE };
typedef long addr_t;
typedef unsigned long size_t;

enum class STAGE : uint32_t {
  READ1,
  READ2,
  READ3,
  READ4,
  COMPT,
  WRITE,
  COMMT,
  SIZE
};
enum class InstState : uint32_t { READY, WAITING, ISSUED, SIZE };

enum class VecMode : uint32_t {
  NEXT_ROW,
  NEXT_LBANK,
  TWOBANK_ONE,
  TWOBANK_TWO
};

struct PeConfig {
  uint32_t id;
  addr_t base;
  addr_t size;
  uint32_t num_sb;
};

enum class PIM_ADDR_MODE : uint32_t { INTLV, BLOCK, SIZE };

long lrand(void);

} // namespace ramulator