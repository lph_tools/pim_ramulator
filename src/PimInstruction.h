#pragma once

#include "common.h"
#include <iostream>

namespace ramulator {

class PimInstructionGenerator;
class PimOp;

class PimInstruction {
public:
  PimInstruction(PimInstructionGenerator *pig, PimOp *pim_op, uint32_t index);

  void setup(PimInstructionGenerator *p, PimOp *po, uint32_t i,
             InstState state);
  void destroy();
  bool execute(STAGE s);
  bool done(STAGE s);
  InstState state();
  void set_state(InstState state);
  PimOp *pim_op();
  uint32_t index();
  PimOpType type();

private:
  PimInstructionGenerator *pig_;
  PimOp *pim_op_;
  uint32_t index_;
  InstState state_;
};

} // namespace ramulator