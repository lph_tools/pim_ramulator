#pragma once

#include "common.h"
#include <cstdlib>
#include <iostream>

namespace ramulator {

class StreamRandomAccess {
public:
  StreamRandomAccess()
      : num_rand_(0), num_stream_(0), stride_(0), addr_(0), stream_cnt_(0),
        random_cnt_(0) {
    std::srand(1);
  }

  StreamRandomAccess(uint32_t num_rand, uint32_t num_stream, uint32_t stride)
      : num_rand_(num_rand), num_stream_(num_stream), stride_(stride),
        stream_cnt_(1), random_cnt_(0) {
    std::srand(1);
    addr_ = lrand();
    ++random_cnt_;
  }

  addr_t next_addr() {
    addr_t next = addr_;
    if (stream_cnt_ >= num_stream_) {
      addr_ = lrand();
      ++random_cnt_;
      stream_cnt_ = 1;
    } else {
      addr_ += stride_;
      ++stream_cnt_;
    }

    return next;
  }

  bool no_more_addr() const { return random_cnt_ > num_rand_; }

private:
  uint32_t num_rand_;
  uint32_t num_stream_;
  uint32_t stride_;
  addr_t addr_;
  uint32_t stream_cnt_;
  uint32_t random_cnt_;
};
} // namespace ramulator