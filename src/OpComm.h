#pragma once

#include "Memory.h"
#include "OpBase.h"
#include "common.h"

namespace ramulator {

class OpBase;
class ProcessingElement;

class OpComm : public OpBase {
public:
  OpComm(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
         PimOpType type = PimOpType::NORMAL);
  virtual std::function<bool(uint32_t)> *done_func() { return done_func_; }
  virtual std::function<bool(uint32_t)> *exec_func() { return exec_func_; }
  virtual uint32_t last_index() { return last_atom_index_; }
  addr_t src_base();
  addr_t dst_base();

private:
  void init_done();
  void init_exec();

  std::function<bool(uint32_t)> done_func_[uint32_t(STAGE::SIZE)];
  std::function<bool(uint32_t)> exec_func_[uint32_t(STAGE::SIZE)];
  uint32_t last_atom_index_;
  uint32_t multiple_;   // used for finding destination PE IDs
  uint32_t distance_;   // distance between src and dst
  uint32_t src_pim_id_; // ID of source PIM (destination is myself)
  addr_t src_base_;
  addr_t dst_base_;
};

} // namespace ramulator
