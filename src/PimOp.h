#pragma once

#include "Memory.h"
#include "OpAxpby.h"
#include "OpAxpbypcz.h"
#include "OpAxpy.h"
#include "OpBase.h"
#include "OpComm.h"
#include "OpCopy.h"
#include "OpDone.h"
#include "OpDot.h"
#include "OpGemv.h"
#include "OpNrm2.h"
#include "OpScal.h"
#include "PimPacket.h"
#include "common.h"

namespace ramulator {

class ProcessingElement;
class OpBase;

class PimOp {
public:
  PimOp(PimPacket &p, MemoryBase *mem, ProcessingElement *pe,
        PimOpType type = PimOpType::NORMAL);
  ~PimOp();

  PimOpType type();
  bool execute(STAGE s, uint32_t index);
  bool done(STAGE s, uint32_t index);
  bool reached_end(uint32_t index);

  enum class OPTYPE : uint32_t {
    AXPBY,
    AXPBYPCZ,
    AXPY,
    COPY,
    DOT,
    DOTC,
    GS,
    JACOBI,
    NRM2,
    SCAL,
    GEMV,
    XMY,
    COMM,
    SIZE
  };

  static std::map<std::string, enum OPTYPE> op_type;

private:
  std::function<bool(uint32_t)> *done_func_;
  std::function<bool(uint32_t)> *exec_func_;

  OpBase *op_;
  // PimPacket op_info_;
  // ProcessingElement* pe_;
};

inline PimOp::PimOp(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
                    PimOpType type)
// : op_info_(p), pe_(pe) {
{
  // op_ = PimOpFactory::create_op(p, mem, pe, type);
  if (type == PimOpType::DONE) {
    op_ = new OpDone(p);
  } else {
    switch (p.op) {
    case uint32_t(OPTYPE::AXPBY):
      op_ = new OpAxpby(p, memory, pe);
      break;
    case uint32_t(OPTYPE::AXPBYPCZ):
      op_ = new OpAxpbypcz(p, memory, pe);
      break;
    case uint32_t(OPTYPE::AXPY):
      op_ = new OpAxpy(p, memory, pe);
      break;
    case uint32_t(OPTYPE::COPY):
      op_ = new OpCopy(p, memory, pe);
      break;
    case uint32_t(OPTYPE::DOT):
      op_ = new OpDot(p, memory, pe);
      break;
    case uint32_t(OPTYPE::DOTC):
      op_ = new OpDot(p, memory, pe);
      break;
    case uint32_t(OPTYPE::GS):
      assert(0);
      break;
    case uint32_t(OPTYPE::JACOBI):
      assert(0);
      break;
    case uint32_t(OPTYPE::NRM2):
      op_ = new OpNrm2(p, memory, pe);
      break;
    case uint32_t(OPTYPE::SCAL):
      op_ = new OpScal(p, memory, pe);
      break;
    case uint32_t(OPTYPE::GEMV):
      op_ = new OpGemv(p, memory, pe);
      break;
    case uint32_t(OPTYPE::XMY):
      // Except for the actual computation, memory access of XMY is exactly the
      // same as AXPBY
      op_ = new OpAxpby(p, memory, pe);
      break;
    case uint32_t(OPTYPE::COMM):
      op_ = new OpComm(p, memory, pe);
      break;
    default:
      std::cout << "[Error] Unknown PIM operation #" << p.op << std::endl;
      exit(EXIT_FAILURE);
      break;
    }
  }

  done_func_ = op_->done_func();
  exec_func_ = op_->exec_func();
}

inline PimOp::~PimOp() { delete op_; }

inline PimOpType PimOp::type() {
  assert(op_);
  return op_->type();
}

inline bool PimOp::done(STAGE s, uint32_t index) {
  return done_func_[uint32_t(s)](index);
}

inline bool PimOp::execute(STAGE s, uint32_t index) {
  if (type() == PimOpType::DONE)
    return true;
  return exec_func_[uint32_t(s)](index);
}

inline bool PimOp::reached_end(uint32_t index) {
  return index >= op_->last_index();
}

} // namespace ramulator
