#include "OpDot.h"
#include "ProcessingElement.h"
namespace ramulator {

OpDot::OpDot(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
             PimOpType type)
    : OpBase(p, memory, pe, type) {
  init_done();
  init_exec();

  if (addr_mode_ == MODE::BLOCK)
    last_atom_index_ = op_info_.size * elem_bytes_ / atom_bytes_;
  else if (addr_mode_ == MODE::INTLV) {
    last_atom_index_ = op_info_.size * elem_bytes_ / atom_bytes_ / 2;

    uint32_t distance = std::abs(x_base_bank_ - y_base_bank_);
    if (distance == 0)
      vec_mode_ = VecMode::NEXT_ROW;
    else if (distance % 2 == 0)
      vec_mode_ = VecMode::TWOBANK_TWO;
    else
      vec_mode_ = VecMode::TWOBANK_ONE;

    base1_ = inverse_translate(base1_, x_base_bank_);
    base2_ = inverse_translate(base2_, x_base_bank_);
    base3_ = inverse_translate(base3_, y_base_bank_);
    base4_ = inverse_translate(base4_, y_base_bank_);
  }
}

void OpDot::init_done() {
  done_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ1);
  };
  done_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ2);
  };
  done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ3);
  };
  done_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ4);
  };
  done_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
}

void OpDot::init_exec() {
  if (addr_mode_ == MODE::BLOCK) {
    exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->pe_->base_addr(), op_info_.base1 + offset,
                               Request::Type::PIMREAD, STAGE::READ1);
    };
    exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->pe_->base_addr(), op_info_.base2 + offset,
                               Request::Type::PIMREAD, STAGE::READ2);
    };
    exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };

  } else if (addr_mode_ == MODE::INTLV) {
    exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->base1(), offset, this->vec_mode(),
                               Request::Type::PIMREAD, STAGE::READ1,
                               x_base_bank_);

    };
    exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->base2(), offset, this->vec_mode(),
                               Request::Type::PIMREAD, STAGE::READ2,
                               x_base_bank_);

    };
    exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->base3(), offset, this->vec_mode(),
                               Request::Type::PIMREAD, STAGE::READ3,
                               y_base_bank_);
    };
    exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->base4(), offset, this->vec_mode(),
                               Request::Type::PIMREAD, STAGE::READ4,
                               y_base_bank_);
    };
    exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
  } else
    assert(0);
}

} // namespace ramulator