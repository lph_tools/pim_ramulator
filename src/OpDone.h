#pragma once

#include "OpBase.h"

namespace ramulator {

class OpDone : public OpBase {
public:
  // OpDone(PimPacket& packet, MemoryBase* memory, ProcessingElement* pe);
  OpDone(PimPacket &packet);
  virtual std::function<bool(uint32_t)> *done_func() { return done_func_; }
  virtual std::function<bool(uint32_t)> *exec_func() { return exec_func_; }
  virtual uint32_t last_index() { return op_info_.size; }

private:
  void init_done();
  void init_exec();
  std::function<bool(uint32_t)> done_func_[uint32_t(STAGE::SIZE)];
  std::function<bool(uint32_t)> exec_func_[uint32_t(STAGE::SIZE)];
};

} // namespace ramulator