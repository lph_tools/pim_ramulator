#pragma once

#include <deque>
#include <iostream>

#include "PimPacket.h"

namespace ramulator {

class PimInstruction;
class PimOp;
class MemoryBase;
class ProcessingElement;

class PimInstructionGenerator {
public:
  PimInstructionGenerator(MemoryBase *memory, ProcessingElement *pe);
  ~PimInstructionGenerator();

  void set_operation(PimPacket &p);
  void push_for_reuse(PimInstruction *p);
  PimInstruction *front();
  void pop();
  bool done();

private:
  MemoryBase *memory_;
  std::deque<PimInstruction *> inst_pool_;
  PimInstruction *front_inst_;
  ProcessingElement *pe_;
  PimOp *op_;
  PimOp *done_op_;
  uint32_t index_;
};

} // namespace ramulator