#pragma once

#include "common.h"
#include <cstdlib>
#include <iostream>

namespace ramulator {

class RandomAccess {
public:
  RandomAccess() : num_(0), idx_(0) { std::srand(1); }
  RandomAccess(uint32_t num) : num_(num), idx_(0) { std::srand(1); }
  addr_t next_addr() {
    ++idx_;
    return lrand();
  }
  bool no_more_addr() const { return idx_ >= num_; }

private:
  uint32_t num_;
  uint32_t idx_;
};
} // namespace ramulator