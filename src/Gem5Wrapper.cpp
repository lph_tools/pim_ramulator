#include <map>
#include <string>

#include "Gem5Wrapper.h"
#include "Config.h"
#include "Request.h"
#include "MemoryFactory.h"
#include "Memory.h"
#include "DDR3.h"
#include "DDR4.h"
#include "LPDDR3.h"
#include "LPDDR4.h"
#include "GDDR5.h"
#include "WideIO.h"
#include "WideIO2.h"
#include "HBM.h"
#include "SALP.h"
#include "PIM.h"
#include "PIM_DDR4.h"
#include "PimPacket.h"
#include "PimTraceGen.h"
#include "PimController.h"

using namespace ramulator;

static map<string, function<MemoryBase *(const Config&, int)> > name_to_func = {
    {"DDR3", &MemoryFactory<DDR3>::create}, {"DDR4", &MemoryFactory<DDR4>::create},
    {"LPDDR3", &MemoryFactory<LPDDR3>::create}, {"LPDDR4", &MemoryFactory<LPDDR4>::create},
    {"GDDR5", &MemoryFactory<GDDR5>::create}, 
    {"WideIO", &MemoryFactory<WideIO>::create}, {"WideIO2", &MemoryFactory<WideIO2>::create},
    {"HBM", &MemoryFactory<HBM>::create},
    {"SALP-1", &MemoryFactory<SALP>::create}, {"SALP-2", &MemoryFactory<SALP>::create}, {"SALP-MASA", &MemoryFactory<SALP>::create},
    // {"HBM2_PIM", &MemoryFactory<HBM2_PIM>::create}, {"DDR4_PIM", &MemoryFactory<DDR4_PIM>::create}, 
    {"PIM", &MemoryFactory<PIM>::create}, {"PIM_DDR4", &MemoryFactory<PIM_DDR4>::create},
};


Gem5Wrapper::Gem5Wrapper(const Config& configs, int cacheline)
{
    const string& std_name = configs["standard"];
    assert(name_to_func.find(std_name) != name_to_func.end() && "unrecognized standard name");
    mem = name_to_func[std_name](configs, cacheline);
    tCK = mem->clk_ns();

    // PIM TRACE GENERATOR
    if (configs["trace_type"] == "PIM") {
        pim_enabled = true;
        std::string pim_tracename = configs["pim_trace_file"];
        if (configs.contains("pimtrace_mode")) {
            if (configs["pimtrace_mode"].compare("real_time") == 0)
                pimtrace_gen = new PimTraceGen(configs, pim_tracename.c_str(), mem);
            else
              pimtrace_gen = new PimTraceGen(pim_tracename.c_str());
        } else {
            // default
            pimtrace_gen = new PimTraceGen(pim_tracename.c_str());
        }
        pim_ctrl = new PimController(configs, mem);

    } else {
        pim_enabled = false;
        pimtrace_gen = nullptr;
        pim_ctrl = nullptr;
    }
}


Gem5Wrapper::~Gem5Wrapper() {
    delete mem;
}

void Gem5Wrapper::tick()
{
    if (pim_enabled) {
        if (pim_ctrl->ready()) {
          PimPacket packet;
          bool trace_end = pimtrace_gen->read_pim_packet(packet);
          packet.print();          
          if (!trace_end)
            pim_ctrl->launch(packet);
        }
        pim_ctrl->tick();
    }
    
    mem->tick();
}

bool Gem5Wrapper::send(Request req)
{
    return mem->send(req);
}

void Gem5Wrapper::finish(void) {
    mem->finish();
}
