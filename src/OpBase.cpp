#include "OpBase.h"
#include "ProcessingElement.h"
#include "Request.h"

#include <cassert>

namespace ramulator {

OpBase::OpBase(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
               PimOpType type)
    : op_info_(p), pe_(pe), elem_bytes_(4), // single precision
      vec_mode_(VecMode::NEXT_ROW), memory_(memory), type_(type) {
  wait_queue_.resize(uint32_t(STAGE::SIZE));
  if (memory_)
    atom_bytes_ = memory_->atom_bytes();

  if (op_info_.mode & 0xf0)
    addr_mode_ = MODE::INTLV;
  else
    addr_mode_ = MODE::BLOCK;

  x_base_bank_ = (op_info_.mode >> 12) & 0xf;
  y_base_bank_ = (op_info_.mode >> 16) & 0xf;
  z_base_bank_ = (op_info_.mode >> 20) & 0xf;
  w_base_bank_ = (op_info_.mode >> 24) & 0xf;

  if (pe_) {
    base1_ = pe_->base_addr() + op_info_.base1;
    base2_ = pe_->base_addr() + op_info_.base2;
    base3_ = pe_->base_addr() + op_info_.base3;
    base4_ = pe_->base_addr() + op_info_.base4;
    baser_ = pe_->base_addr() + op_info_.baser;
    num_local_bank_ = pe->num_local_sb();
  }
}

bool OpBase::issue_memop(addr_t base_addr, addr_t offset, Request::Type type,
                         STAGE stage) {
  // check whether accessing out of local memory boundary
  assert(offset < pe_->size());

  addr_t target_addr = get_addr(base_addr, offset);
  Request req(target_addr, type, int32_t(addr_mode_),
              [stage, this](Request &r) { this->remove_req(stage, r); });

  if (memory_->send(req)) {
#ifdef DEBUG
    std::string type_str = (type == Request::Type::PIMREAD) ? "READ" : "WRITE";
    std::cout << "--> Send Request: " << std::hex << "0x" << target_addr
              << std::dec << ", " << type_str << std::endl;
#endif
    wait_queue_[uint32_t(stage)].push_back(target_addr);
    return true;
  }

  return false;
}

bool OpBase::issue_memop(addr_t target_addr, MODE mode, Request::Type type,
                         STAGE stage) {

  Request req(target_addr, type, int32_t(mode),
              [stage, this](Request &r) { this->remove_req(stage, r); });

  if (memory_->send(req)) {
#ifdef DEBUG
    std::string type_str = (type == Request::Type::PIMREAD) ? "READ" : "WRITE";
    std::cout << "--> Send Request: " << std::hex << "0x" << target_addr
              << std::dec << ", " << type_str << std::endl;
#endif
    wait_queue_[uint32_t(stage)].push_back(target_addr);
    return true;
  }

  return false;
}

bool OpBase::issue_memop(addr_t base_addr, addr_t offset, VecMode vec_mode,
                         Request::Type type, STAGE stage, uint32_t base_bank) {
  addr_t target_addr = get_addr(base_addr, offset, vec_mode);
  addr_t physical_addr = translate(target_addr, base_bank);
  Request req(physical_addr, type,
              [stage, this](Request &r) { this->remove_req(stage, r); });

  if (memory_->send(req)) {
#ifdef DEBUG
    std::string type_str = (type == Request::Type::PIMREAD) ? "READ" : "WRITE";
    std::cout << "--> Send Request: " << std::hex << "0x" << physical_addr
              << std::dec << ", " << type_str << std::endl;
#endif
    wait_queue_[uint32_t(stage)].push_back(physical_addr);
    return true;
  }

  return false;
}

void OpBase::remove_req(STAGE stage, Request &r) {
  auto it = std::find(wait_queue_[uint32_t(stage)].begin(),
                      wait_queue_[uint32_t(stage)].end(), r.addr);

  assert(it != wait_queue_[uint32_t(stage)].end());

  wait_queue_[uint32_t(stage)].erase(it);
}

addr_t OpBase::get_addr(addr_t base_addr, addr_t offset) {
  assert(offset < pe_->size());

  return base_addr + offset;
}

// implemented for interleave mode only
// vec_mode
// 0: next row of the same local bank
// 1: same row of the next local bank
// 2: group two adjacent local banks and interleave within the group
// 3: group two local banks that are one bank apart and interleave
addr_t OpBase::get_addr(addr_t base_addr, addr_t offset, VecMode vec_mode) {
  uint32_t row_sz = memory_->row_size();
  uint32_t row_offset = offset / row_sz;
  uint32_t col_offset = offset - row_sz * row_offset;
  uint32_t bnk_offset = -1;
  addr_t row_addr = (addr_t)-1;

  switch (vec_mode) {
  case VecMode::NEXT_ROW:
    row_addr = memory_->add_to_row_addr(base_addr, row_offset);
    break;

  case VecMode::NEXT_LBANK:
    row_addr = base_addr + row_offset * row_sz;
    break;

  case VecMode::TWOBANK_ONE: {
    bnk_offset = row_offset % (num_local_bank_ / 2);
    row_addr = base_addr;
    uint32_t lbank_idx = memory_->local_bank_index(row_addr);
    if (bnk_offset == 1) {
      row_addr = memory_->add_to_bank_addr(row_addr, 1);
      if (lbank_idx == num_local_bank_ - 1) {
        row_addr = memory_->add_to_row_addr(row_addr, row_offset / 2 + 1);
        break;
      }
    }
    row_addr = memory_->add_to_row_addr(row_addr, row_offset / 2);
    break;
  }
  case VecMode::TWOBANK_TWO: {
    bnk_offset = row_offset % (num_local_bank_ / 2);
    row_addr = base_addr;
    uint32_t lbank_idx = memory_->local_bank_index(row_addr);
    if (bnk_offset == 1) {
      row_addr = memory_->add_to_bank_addr(row_addr, 2);
      if (lbank_idx >= num_local_bank_ - 2) {
        row_addr = memory_->add_to_row_addr(row_addr, row_offset / 2 + 1);
        break;
      }
    }
    row_addr = memory_->add_to_row_addr(row_addr, row_offset / 2);
    break;
  }

  default:
    break;
  }

  return row_addr + col_offset;
}

bool OpBase::mem_done(STAGE stage) {
  return wait_queue_[uint32_t(stage)].empty();
}

PimOpType OpBase::type() { return type_; }

addr_t OpBase::translate(addr_t addr, uint32_t base_bank) {
  uint32_t bank_idx = memory_->local_bank_index(addr);
  addr_t physical_addr;

  if (bank_idx < base_bank)
    physical_addr = memory_->add_to_row_addr(addr, -1);
  else
    physical_addr = addr;

  return physical_addr;
}

addr_t OpBase::inverse_translate(addr_t addr, uint32_t base_bank) {
  uint32_t bank_idx = memory_->local_bank_index(addr);
  addr_t logical_addr;

  if (bank_idx < base_bank)
    logical_addr = memory_->add_to_row_addr(addr, 1);
  else
    logical_addr = addr;

  return logical_addr;
}

addr_t OpBase::base1() { return base1_; }
addr_t OpBase::base2() { return base2_; }
addr_t OpBase::base3() { return base3_; }
addr_t OpBase::base4() { return base4_; }
addr_t OpBase::baser() { return baser_; }
VecMode OpBase::vec_mode() { return vec_mode_; }

} // namespace ramulator