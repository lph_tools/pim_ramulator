#pragma once

#include <iostream>

#include "Config.h"
#include "Memory.h"
#include "ProcessingElement.h"

namespace ramulator {

class PimController {
public:
  ScalarStat pim_bytes;

  PimController(const Config &config, MemoryBase *memory);
  ~PimController();
  bool busy();
  void launch(PimPacket &packet);
  bool ready();
  void tick();
  addr_t base_addr(uint32_t pe_id);

private:
  addr_t get_base_addr(uint32_t pe_id, uint32_t num_bank_per_pe);

  enum class STATE : uint32_t { READY, BUSY, SIZE } state_ = STATE::READY;
  MemoryBase *memory_;
  std::vector<ProcessingElement *> pe_vec_;
};

} // namespace ramulator
