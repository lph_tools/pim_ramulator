#pragma once
#include "common.h"
#include <iostream>
#include <memory>

namespace ramulator {

class HostAccessGen {
public:
  HostAccessGen() {}

  template <typename T>
  HostAccessGen(T concrete_gen)
      : concrete_gen_(
            std::make_unique<concrete_addr_gen_t<T>>(std::move(concrete_gen))) {
  }

  HostAccessGen(const HostAccessGen &that)
      : concrete_gen_(that.concrete_gen_->copy_()) {}

  HostAccessGen(HostAccessGen &&that) noexcept = default;

  HostAccessGen &operator=(const HostAccessGen &that) {
    HostAccessGen tmp(that);
    *this = std::move(tmp);
    return *this;
  }

  HostAccessGen &operator=(HostAccessGen &&that) noexcept = default;

  addr_t next_addr() { return concrete_gen_->next_addr(); }
  bool no_more_addr() const { return concrete_gen_->no_more_addr(); }

private:
  struct addr_gen_t {
    virtual ~addr_gen_t() = default;
    virtual std::unique_ptr<addr_gen_t> copy_() const = 0;
    virtual addr_t next_addr() = 0;
    virtual bool no_more_addr() const = 0;
  };

  template <typename T> struct concrete_addr_gen_t final : addr_gen_t {
    concrete_addr_gen_t(T x) : conc_gen_(std::move(x)) {}
    std::unique_ptr<addr_gen_t> copy_() const override {
      return std::make_unique<concrete_addr_gen_t>(*this);
    }

    addr_t next_addr() override { return conc_gen_.next_addr(); }
    bool no_more_addr() const override { return conc_gen_.no_more_addr(); }

    T conc_gen_;
  };

  std::unique_ptr<addr_gen_t> concrete_gen_;
};

} // namespace ramulator