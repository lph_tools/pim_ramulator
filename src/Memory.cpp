#include "Memory.h"
#include <iterator>

namespace ramulator {

template <> std::vector<DDR4::Level> Memory<DDR4>::get_levels(Type type) const {
  switch(int(type)) {
    case int(Type::ChRoRaBaCo): {
      std::vector<DDR4::Level> ret_vec({DDR4::Level::Channel, 
                                        DDR4::Level::Row, 
                                        DDR4::Level::Rank, 
                                        DDR4::Level::BankGroup, 
                                        DDR4::Level::Bank, 
                                        DDR4::Level::Column});
      return ret_vec;
      break;
    }
    case int(Type::ChRaRoBaCo): {
      std::vector<DDR4::Level> ret_vec({DDR4::Level::Channel, 
                                        DDR4::Level::Rank, 
                                        DDR4::Level::Row, 
                                        DDR4::Level::BankGroup, 
                                        DDR4::Level::Bank, 
                                        DDR4::Level::Column});
      return ret_vec;
      break;
    }
    default:
      assert(false && "[Error] Unexpected type.");
      break;
  } 
}

template <> std::vector<PIM::Level> Memory<PIM>::get_levels(Type type) const {
  PIM::Level mapping_levels[int(Type::MAX)][int(PIM::Level::MAX)] = {
      {PIM::Level::Channel, PIM::Level::Rank, PIM::Level::PimGroup,
       PIM::Level::BankGroup, PIM::Level::Bank, PIM::Level::SubBank,
       PIM::Level::Row, PIM::Level::Column},
      {PIM::Level::Row, PIM::Level::PimGroup, PIM::Level::BankGroup,
       PIM::Level::Bank, PIM::Level::SubBank, PIM::Level::Rank,
       PIM::Level::Column, PIM::Level::Channel},
      {PIM::Level::BankGroup, PIM::Level::Bank, PIM::Level::SubBank,
       PIM::Level::Row, PIM::Level::PimGroup, PIM::Level::Rank,
       PIM::Level::Column, PIM::Level::Channel}, // HOST_BLOCK
      {PIM::Level::Row, PIM::Level::BankGroup, PIM::Level::Bank,
       PIM::Level::SubBank, PIM::Level::PimGroup, PIM::Level::Rank,
       PIM::Level::Column, PIM::Level::Channel}, // HOST_INTLV
      {PIM::Level::Channel, PIM::Level::Rank, PIM::Level::PimGroup,
       PIM::Level::BankGroup, PIM::Level::Bank, PIM::Level::SubBank,
       PIM::Level::Row, PIM::Level::Column}, // PIM_BLOCK_FB
      {PIM::Level::Channel, PIM::Level::Rank, PIM::Level::PimGroup,
       PIM::Level::BankGroup, PIM::Level::Bank, PIM::Level::SubBank,
       PIM::Level::Row, PIM::Level::Column}, // PIM_BLOCK_TBTSB
      {PIM::Level::Channel, PIM::Level::Rank, PIM::Level::PimGroup,
       PIM::Level::BankGroup, PIM::Level::Bank, PIM::Level::SubBank,
       PIM::Level::Row, PIM::Level::Column}, // PIM_BLOCK_OBFSB
      {PIM::Level::Channel, PIM::Level::Rank, PIM::Level::PimGroup,
       PIM::Level::Row, PIM::Level::BankGroup, PIM::Level::Bank,
       PIM::Level::SubBank, PIM::Level::Column}, // PIM_INTLV_FB
      {PIM::Level::Channel, PIM::Level::Rank, PIM::Level::PimGroup,
       PIM::Level::BankGroup, PIM::Level::Row, PIM::Level::Bank,
       PIM::Level::SubBank, PIM::Level::Column}, // PIM_INTLV_TBTSB
      {PIM::Level::Channel, PIM::Level::Rank, PIM::Level::PimGroup,
       PIM::Level::BankGroup, PIM::Level::Bank, PIM::Level::Row,
       PIM::Level::SubBank, PIM::Level::Column} // PIM_INTLV_OBFSB
  };

  std::vector<PIM::Level> ret_vec(std::begin(mapping_levels[int(type)]),
                                  std::end(mapping_levels[int(type)]));

  return ret_vec;
}

template <>
long Memory<PIM>::decode(const std::vector<int> &addr_vec, Type type) const {
  long ret_addr = 0l;

  vector<PIM::Level> levels = get_levels(type);
  for (int i = 0, size = levels.size(); i < size; ++i) {
    int level = int(levels[i]);
    ret_addr <<= int(addr_bits[level]);
    ret_addr |= addr_vec[level];
  }
  ret_addr <<= calc_log2(spec->org_entry.dq / 8);

  return ret_addr;
}

template <>
void Memory<PIM>::encode(std::vector<int> &addr_vec, long addr,
                         Type type) const {
  vector<PIM::Level> levels = get_levels(type);
  clear_lower_bits(addr, calc_log2(spec->channel_width / 8));

  for (int i = levels.size() - 1; i >= 0; --i) {
    int level = int(levels[i]);
    addr_vec[level] = slice_lower_bits(addr, addr_bits[level]);
  }
}

template <> long Memory<PIM>::bank_addr(long bank_id) const {
  long curr_id = bank_id;
  std::vector<int> addr_vec(addr_bits.size(), 0);

  for (int i = int(PIM::Level::Bank); i >= int(PIM::Level::Channel); i--)
    addr_vec[i] = slice_lower_bits(curr_id, addr_bits[i]);

  return decode(addr_vec, pim_addr_type);
}

template <> bool Memory<DDR4>::send(Request req) {
  req.addr_vec.resize(addr_bits.size());
  long addr = req.addr;
  int coreid = req.coreid;

  // translating address mapping type
  encode(req.addr_vec, addr, type);

  if (ctrls[req.addr_vec[0]]->enqueue(req)) {
    // tally stats here to avoid double counting for requests that aren't
    // enqueued
    ++num_incoming_requests;
    if (req.type == Request::Type::READ) {
      ++num_read_requests[coreid];
      ++incoming_read_reqs_per_channel[req.addr_vec[int(DDR4::Level::Channel)]];
    }
    if (req.type == Request::Type::WRITE) {
      ++num_write_requests[coreid];
    }
    ++incoming_requests_per_channel[req.addr_vec[int(DDR4::Level::Channel)]];
    return true;
  }

  return false;
}

template <> bool Memory<PIM>::send(Request req) {
  req.addr_vec.resize(addr_bits.size());
  long addr = req.addr;
  int coreid = req.coreid;

  // translating address mapping type
  Type addr_type;
  // if (req.type == Request::Type::READ ||
  //     req.type == Request::Type::WRITE) // Host access
  //   addr_type = type;
  // else if (req.addr_type == 0) // PIM access in block mode
  //   addr_type = Type::BLOCK_PIM;
  // else if (req.addr_type == 1) // PIM access with given configuration
  //   addr_type = pim_addr_type;
  // else
  //   addr_type = Type::MAX;

  if (req.type == Request::Type::READ ||
      req.type == Request::Type::WRITE) // Host access
    addr_type = type;
  else
    addr_type = pim_addr_type;

  assert(addr_type != Type::MAX);

  encode(req.addr_vec, addr, addr_type);

  if (ctrls[req.addr_vec[0]]->enqueue(req)) {
    // tally stats here to avoid double counting for requests that aren't
    // enqueued
    ++num_incoming_requests;
    if (req.type == Request::Type::READ) {
      ++num_read_requests[coreid];
      ++incoming_read_reqs_per_channel[req.addr_vec[int(PIM::Level::Channel)]];
    }
    if (req.type == Request::Type::WRITE) {
      ++num_write_requests[coreid];
    }
    ++incoming_requests_per_channel[req.addr_vec[int(PIM::Level::Channel)]];
    return true;
  }

  return false;
}

template <> long Memory<PIM>::add_to_bank_addr(long base, long bank_inc) const {
  std::vector<int> addr_vec(addr_bits.size(), 0);
  encode(addr_vec, base, pim_addr_type);

  if (pim_addr_type == Type::INTLV_PIM_FB ||
      pim_addr_type == Type::BLOCK_PIM_FB) {
    long bank_id = (addr_vec[int(PIM::Level::BankGroup)]
                    << addr_bits[uint32_t(PIM::Level::Bank)]) +
                   addr_vec[int(PIM::Level::Bank)];
    bank_id += bank_inc;

    addr_vec[int(PIM::Level::Bank)] =
        slice_lower_bits(bank_id, addr_bits[uint32_t(PIM::Level::Bank)]);
    addr_vec[int(PIM::Level::BankGroup)] =
        slice_lower_bits(bank_id, addr_bits[uint32_t(PIM::Level::BankGroup)]);
  } else if (pim_addr_type == Type::INTLV_PIM_TBTSB ||
             pim_addr_type == Type::BLOCK_PIM_TBTSB) {
    long bank_id = (addr_vec[int(PIM::Level::Bank)]
                    << addr_bits[uint32_t(PIM::Level::SubBank)]) +
                   addr_vec[int(PIM::Level::SubBank)];
    bank_id += bank_inc;
    addr_vec[int(PIM::Level::SubBank)] =
        slice_lower_bits(bank_id, addr_bits[uint32_t(PIM::Level::SubBank)]);
    addr_vec[int(PIM::Level::Bank)] =
        slice_lower_bits(bank_id, addr_bits[uint32_t(PIM::Level::Bank)]);
    // addr_vec[int(PIM::Level::Row)] += bank_id;

  } else if (pim_addr_type == Type::INTLV_PIM_OBFSB ||
             pim_addr_type == Type::BLOCK_PIM_OBFSB) {
    addr_vec[int(PIM::Level::SubBank)] += bank_inc;

  } else
    addr_vec[int(PIM::Level::Bank)] += bank_inc;

  // addr_vec[int(PIM::Level::SubBank)] +=
  //     slice_lower_bits(bank_inc, addr_bits[uint32_t(PIM::Level::SubBank)]);
  // addr_vec[int(PIM::Level::Bank)] +=
  //     slice_lower_bits(bank_inc, addr_bits[uint32_t(PIM::Level::Bank)]);
  // addr_vec[int(PIM::Level::BankGroup)] += bank_inc;

  return decode(addr_vec, pim_addr_type);
}

template <> uint32_t Memory<PIM>::local_bank_index(addr_t addr) const {
  std::vector<int> addr_vec(addr_bits.size(), 0);
  encode(addr_vec, addr, pim_addr_type);

  uint32_t bank_idx = -1;

  if (pim_addr_type == Type::INTLV_PIM_FB)
    // bank_idx = addr_vec[int(PIM::Level::Bank)];
    bank_idx = (addr_vec[int(PIM::Level::BankGroup)]
                << addr_bits[uint32_t(PIM::Level::Bank)]) +
               addr_vec[int(PIM::Level::Bank)];
  else if (pim_addr_type == Type::INTLV_PIM_TBTSB) {
    bank_idx = (addr_vec[int(PIM::Level::Bank)]
                << addr_bits[uint32_t(PIM::Level::SubBank)]) +
               addr_vec[int(PIM::Level::SubBank)];
  } else if (pim_addr_type == Type::INTLV_PIM_OBFSB)
    bank_idx = addr_vec[int(PIM::Level::SubBank)];
  else
    bank_idx = addr_vec[int(PIM::Level::Bank)];

  return bank_idx;
}

} // namespace ramulator