#pragma once

#include "Memory.h"
#include "PimPacket.h"
#include "common.h"

namespace ramulator {

class ProcessingElement;

class OpBase {
public:
  virtual std::function<bool(uint32_t)> *done_func() = 0;
  virtual std::function<bool(uint32_t)> *exec_func() = 0;
  virtual uint32_t last_index() = 0;
  PimOpType type();
  virtual ~OpBase() {}

  enum class MODE : uint32_t { BLOCK, INTLV, SIZE };

  addr_t base1();
  addr_t base2();
  addr_t base3();
  addr_t base4();
  addr_t baser();

protected:
  // enum class MemOpType { READ, WRITE, SIZE };
  OpBase(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
         PimOpType type = PimOpType::NORMAL);

  bool issue_memop(addr_t base_addr, addr_t offset, Request::Type type,
                   STAGE stage);
  bool issue_memop(addr_t target_addr, MODE mode, Request::Type type,
                   STAGE stage);
  bool issue_memop(addr_t base_addr, addr_t offset, VecMode mode,
                   Request::Type type, STAGE stage, uint32_t base_bank);
  addr_t get_addr(addr_t base_addr, addr_t offset);
  addr_t get_addr(addr_t base_addr, addr_t offset, VecMode vec_mode);
  bool mem_done(STAGE stage);
  void remove_req(STAGE stage, Request &r);
  VecMode vec_mode();
  addr_t inverse_translate(addr_t addr, uint32_t base_bank);

  PimPacket op_info_;
  ProcessingElement *pe_;
  uint32_t atom_bytes_;
  const uint32_t elem_bytes_;
  MODE addr_mode_;
  uint32_t x_base_bank_;
  uint32_t y_base_bank_;
  uint32_t z_base_bank_;
  uint32_t w_base_bank_;
  VecMode vec_mode_;
  addr_t base1_;
  addr_t base2_;
  addr_t base3_;
  addr_t base4_;
  addr_t baser_;

private:
  MemoryBase *memory_;
  std::vector<std::vector<addr_t>> wait_queue_;
  PimOpType type_;
  uint32_t num_local_bank_;

  addr_t translate(addr_t addr, uint32_t base_bank);
};

} // namespace ramulator