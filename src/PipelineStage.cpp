#include "PipelineStage.h"
#include "PimInstruction.h"
#include "PimOp.h"
#include <cassert>

namespace ramulator {

std::string stage_name[uint32_t(STAGE::SIZE)] = {
    "READ1", "READ2", "READ3", "READ4", "COMPT", "WRITE", "COMMT"};

PipelineStage::PipelineStage(STAGE stage, STAGE prev_stage, PipelineStage *next,
                             uint32_t num_slots)
    : stage_(stage), prev_stage_(prev_stage), next_(next),
      slot_capacity_(num_slots) {
  slot_.resize(num_slots);
  reset();
}

PipelineStage::~PipelineStage() {
  for (auto it = slot_.begin(); it != slot_.end(); ++it)
    if (*it)
      delete *it;
}

// return true if last instruction is done
bool PipelineStage::tick() {
  bool ret_val = false;

  if (slot_[0] != nullptr) {
    PimInstruction *p = slot_[0];

    if (p->state() == InstState::ISSUED) {
      assert(stage_ != STAGE::READ1);
      if (p->done(prev_stage_))
        p->set_state(InstState::READY);
      else
        return false;
    }

    if (p->state() == InstState::READY) {
      if (p->execute(stage_))
        p->set_state(InstState::ISSUED);

      if (next_ == nullptr) {                       // last pipeline stage
        if (p->pim_op()->type() == PimOpType::DONE) // end of instruction stream
          ret_val = true;
        p->destroy();
        slot_[0] = nullptr;
      } else if (next_->add_instruction(p)) {
        slot_[0] = nullptr;
      } else {
        p->set_state(InstState::WAITING);
        return false;
      }

#ifdef DEBUG
      if (slot_[0] == nullptr)
        std::cout << "[Index: " << p->index() << "]["
                  << stage_name[uint32_t(stage_)] << "] done" << std::endl;
#endif
    }

    if (p->state() == InstState::WAITING) {
      if (next_->add_instruction(p)) {
        p->set_state(InstState::ISSUED);
        slot_[0] = nullptr;
      } else {
        return false;
      }
    }

    // if (p->state() == InstState::WAITING)
    //   if (p->done(stage_))
    //     p->set_state(InstState::DONE);

    // if (p->state() == InstState::DONE) {
    //   if (next_ == nullptr) {                       // last pipeline stage
    //     if (p->pim_op()->type() == PimOpType::DONE) // end of instruction
    //     stream
    //       ret_val = true;
    //     p->destroy();
    //     slot_[0] = nullptr;
    //   } else if (next_->add_instruction(p)) {
    //     slot_[0] = nullptr;
    //   }

    //   if (slot_[0] == nullptr)
    //     std::cout << "[" << stage_name[uint32_t(stage_)] << "] done"
    //               << std::endl;
    // }
  }

  for (uint32_t i = 1; i < slot_capacity_; i++)
    if (slot_[i - 1] == nullptr) {
      slot_[i - 1] = slot_[i];
      slot_[i] = nullptr;
    }

  return ret_val;
}

void PipelineStage::reset() {
  assert(slot_.size() != 0);

  for (uint32_t i = 0; i < slot_capacity_; i++)
    slot_[i] = nullptr;
}

bool PipelineStage::add_instruction(PimInstruction *p) {
  if (slot_[slot_capacity_ - 1] != nullptr)
    return false;

  // p->set_state(InstState::READY);
  slot_[slot_capacity_ - 1] = p;

  return true;
}

void PipelineStage::set_next(PipelineStage *next) { next_ = next; }

} // namespace ramulator