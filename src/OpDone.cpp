#include "OpDone.h"
#include "ProcessingElement.h"

namespace ramulator {

OpDone::OpDone(PimPacket &packet)
    : OpBase(packet, nullptr, nullptr, PimOpType::DONE) {
  init_done();
  init_exec();
}

void OpDone::init_done() {
  done_func_[uint32_t(STAGE::READ1)] = [this](uint32_t index) { return true; };
  done_func_[uint32_t(STAGE::READ2)] = [this](uint32_t index) { return true; };
  done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t index) { return true; };
  done_func_[uint32_t(STAGE::READ4)] = [this](uint32_t index) { return true; };
  done_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t index) { return true; };
  done_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t index) { return true; };
}

void OpDone::init_exec() {
  exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t index) { return true; };
  exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t index) { return true; };
  exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t index) { return true; };
  exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t index) { return true; };
  exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t index) { return true; };
  exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t index) { return true; };
}

} // namespace ramulator