#include "OpNrm2.h"
#include "ProcessingElement.h"
namespace ramulator {

OpNrm2::OpNrm2(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
               PimOpType type)
    : OpBase(p, memory, pe, type) {
  init_done();
  init_exec();

  if (addr_mode_ == MODE::BLOCK) {
    last_atom_index_ = op_info_.size * elem_bytes_ / atom_bytes_;
  } else if (addr_mode_ == MODE::INTLV) {
    last_atom_index_ = op_info_.size * elem_bytes_ / atom_bytes_ / 4;

    base1_ = inverse_translate(base1_, x_base_bank_);
    base2_ = inverse_translate(base2_, x_base_bank_);
    base3_ = inverse_translate(base3_, x_base_bank_);
    base4_ = inverse_translate(base4_, x_base_bank_);
  }
}

void OpNrm2::init_done() {
  done_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ1);
  };
  done_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
}

void OpNrm2::init_exec() {
  if (addr_mode_ == MODE::BLOCK) {
    exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->base1() + offset, MODE::BLOCK,
                               Request::Type::PIMREAD, STAGE::READ1);
    };
    exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };

  } else if (addr_mode_ == MODE::INTLV) {
    exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      bool issued = true;
      issued &=
          this->issue_memop(this->base1(), offset, vec_mode_,
                            Request::Type::PIMREAD, STAGE::READ1, x_base_bank_);
      issued &=
          this->issue_memop(this->base2(), offset, vec_mode_,
                            Request::Type::PIMREAD, STAGE::READ1, x_base_bank_);
      issued &=
          this->issue_memop(this->base3(), offset, vec_mode_,
                            Request::Type::PIMREAD, STAGE::READ1, x_base_bank_);
      issued &=
          this->issue_memop(this->base4(), offset, vec_mode_,
                            Request::Type::PIMREAD, STAGE::READ1, x_base_bank_);
      assert(issued);
      return true;
    };
    exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
  } else
    assert(0);
}

} // namespace ramulator