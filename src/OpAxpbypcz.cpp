#include "OpAxpbypcz.h"
#include "ProcessingElement.h"
namespace ramulator {

OpAxpbypcz::OpAxpbypcz(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
                       PimOpType type)
    : OpBase(p, memory, pe, type),
      last_atom_index_(op_info_.size * elem_bytes_ / atom_bytes_) {
  init_done();
  init_exec();

  if (addr_mode_ == MODE::INTLV) {
    vec_mode_ = VecMode::NEXT_LBANK;

    base1_ = inverse_translate(base1_, x_base_bank_);
    base2_ = inverse_translate(base2_, y_base_bank_);
    base3_ = inverse_translate(base3_, z_base_bank_);
    baser_ = inverse_translate(baser_, w_base_bank_);
  }
}

void OpAxpbypcz::init_done() {
  done_func_[uint32_t(STAGE::READ1)] = [this](uint32_t index) {
    return this->mem_done(STAGE::READ1);
  };
  done_func_[uint32_t(STAGE::READ2)] = [this](uint32_t index) {
    return this->mem_done(STAGE::READ2);
  };
  done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t index) {
    return this->mem_done(STAGE::READ3);
  };
  done_func_[uint32_t(STAGE::READ4)] = [this](uint32_t index) { return true; };
  done_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t index) { return true; };
  done_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t index) {
    return this->mem_done(STAGE::WRITE);
  };
  done_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t index) { return true; };
}

void OpAxpbypcz::init_exec() {
  exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t index) {
    addr_t offset = index * this->atom_bytes_;
    return this->issue_memop(this->base1(), offset, this->vec_mode(),
                             Request::Type::PIMREAD, STAGE::READ1,
                             x_base_bank_);
  };
  exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t index) {
    addr_t offset = index * this->atom_bytes_;
    return this->issue_memop(this->base2(), offset, this->vec_mode(),
                             Request::Type::PIMREAD, STAGE::READ2,
                             y_base_bank_);
  };
  exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t index) {
    addr_t offset = index * this->atom_bytes_;
    return this->issue_memop(this->base3(), offset, this->vec_mode(),
                             Request::Type::PIMREAD, STAGE::READ3,
                             z_base_bank_);
  };
  exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t index) { return true; };
  exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t index) { return true; };
  exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t index) {
    addr_t offset = index * this->atom_bytes_;
    return this->issue_memop(this->baser(), offset, this->vec_mode(),
                             Request::Type::PIMWRITE, STAGE::WRITE,
                             w_base_bank_);
  };
  exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t index) { return true; };
}

} // namespace ramulator