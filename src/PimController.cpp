#include "PimController.h"

namespace ramulator {

PimController::PimController(const Config &config, MemoryBase *memory)
    : memory_(memory) {
  // construct processing elements
  uint32_t num_bank_per_pe, num_lbank_per_pe;
  try {
    num_bank_per_pe = std::stoi(config["pim_granularity"]);
    num_lbank_per_pe = std::stoi(config["num_local_bank"]);
  } catch (...) {
    std::cout << "[Error] check the pim_granularity and num_local_bank setting"
              << std::endl;
    exit(EXIT_FAILURE);
  }

  uint32_t num_pe = memory->num_total_bank() / num_bank_per_pe;

  for (uint32_t i = 0; i < num_pe; i++) {
    addr_t base = get_base_addr(i, num_bank_per_pe);
    addr_t size = memory->bank_capacity() * num_bank_per_pe;
    PeConfig pe_config{i, base, size, num_lbank_per_pe};
    ProcessingElement *pe =
        new ProcessingElement(config, memory, pe_config, this);
    pe_vec_.push_back(pe);
  }

  // register stats
  pim_bytes.name("pim_bytes")
      .desc("Minimum number of bytes that should be accessed during PIM")
      .precision(0);
}

PimController::~PimController() {
  for (auto it = pe_vec_.begin(); it != pe_vec_.end(); ++it)
    delete *it;
}

bool PimController::busy() { return state_ == STATE::BUSY; }

bool PimController::ready() { return state_ == STATE::READY; }

void PimController::launch(PimPacket &packet) {
  state_ = STATE::BUSY;
  uint32_t num_elem = packet.size;

  // update stats
  switch (packet.op) {
  case 0:  // AXPBY
  case 2:  // AXPY
  case 11: // XMY
    num_elem *= 3;
    break;
  case 1: // AXPBY
    num_elem *= 4;
    break;
  case 3:  // COPY
  case 4:  // DOT
  case 5:  // DOTC
  case 9:  // SCAL
  case 12: // COMM
    num_elem *= 2;
    break;
  case 6: // JACOBI
  case 7: // GS
    assert(0);
    break;
  case 8:
    num_elem *= 1;
    break;
  case 10: // GEMV
    num_elem = (packet.size + 1) * (packet.arg1 + 1) - 1;
    break;
  default:
    num_elem = 0;
    break;
  }

  // if (packet.op == 0xa) { // gemv
  //   num_elem = (packet.size + 1) * (packet.arg1 + 1) - 1;
  // }
  const uint32_t elem_bytes = 4; // number of bytes per element
                                 // (single precision -> 4)
  pim_bytes += num_elem * elem_bytes * pe_vec_.size();

  // launch PIM operation
  for (auto it = pe_vec_.begin(); it != pe_vec_.end(); ++it) {
    (*it)->launch(packet);
  }
}

void PimController::tick() {
  if (state_ == STATE::READY)
    return;

  bool done = true;
  for (auto it = pe_vec_.begin(); it != pe_vec_.end(); ++it) {
    done &= (*it)->tick();
  }

  if (done)
    state_ = STATE::READY;
}

addr_t PimController::get_base_addr(uint32_t pe_id, uint32_t num_bank_per_pe) {
  assert(memory_);

  addr_t bank_index = pe_id * num_bank_per_pe;

  return memory_->bank_addr(bank_index);
}

addr_t PimController::base_addr(uint32_t pe_id) {
  return pe_vec_[pe_id]->base_addr();
}

} // namespace ramulator