#pragma once

#include <iostream>

#include "Memory.h"
#include "OpBase.h"
#include "PimInstructionGenerator.h"
#include "PipelineStage.h"
#include "common.h"

namespace ramulator {

class OpBase;
class PimController;

class ProcessingElement {
public:
  ProcessingElement(const Config &config, MemoryBase *memory,
                    const PeConfig &pe_cfg, PimController *pim_ctrl);
  ~ProcessingElement();
  void launch(PimPacket &p);
  bool tick();
  bool done();
  addr_t base_addr();
  addr_t size();
  uint32_t num_local_sb();
  uint32_t id();
  PimController *pim_ctrl();

private:
  void reset();
  void update_next();

  addr_t base_addr_;
  addr_t size_;
  uint32_t id_;
  uint32_t num_local_sb_;
  std::vector<PipelineStage> pipeline_;
  bool done_;
  PimInstructionGenerator pim_inst_gen_;
  PimController *pim_ctrl_;
};

} // namespace ramulator
