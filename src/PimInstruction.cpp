#include "PimInstruction.h"
#include "PimInstructionGenerator.h"
#include "PimOp.h"

namespace ramulator {

PimInstruction::PimInstruction(PimInstructionGenerator *pig, PimOp *pim_op,
                               uint32_t index) {
  setup(pig, pim_op, index, InstState::READY);
}

void PimInstruction::setup(PimInstructionGenerator *p, PimOp *po, uint32_t i,
                           InstState state) {
  assert(p);
  assert(po);

  pig_ = p;
  pim_op_ = po;
  index_ = i;
  state_ = state;
}

void PimInstruction::destroy() {
  assert(pig_);
  // will be destructed by pig later at once
  pig_->push_for_reuse(this);
}

bool PimInstruction::execute(STAGE s) {
  assert(pim_op_);
  return pim_op_->execute(s, index_);
}
bool PimInstruction::done(STAGE s) {
  assert(pim_op_);
  return pim_op_->done(s, index_);
}

InstState PimInstruction::state() { return state_; }
void PimInstruction::set_state(InstState state) { state_ = state; }
PimOp *PimInstruction::pim_op() { return pim_op_; }
uint32_t PimInstruction::index() { return index_; }
PimOpType PimInstruction::type() { return pim_op_->type(); }

} // namespace ramulator