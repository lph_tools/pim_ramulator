#pragma once

#include "common.h"
#include <iostream>

namespace ramulator {

struct PimPacket {
  enum class Type { PACKET_HEAD, PACKET_BODY, PACKET_TAIL, SIZE };

  unsigned int mode;
  unsigned int op;
  addr_t base1;
  addr_t base2;
  addr_t base3;
  addr_t base4;
  addr_t baser;
  addr_t size;
  addr_t arg1;
  unsigned int arg2;
  unsigned int arg3;

  void print() {
    std::cout << "\n-----[Packet info]-----" << std::endl;
    std::cout << "mode: 0x" << std::hex << this->mode << std::endl;
    std::cout << "op: 0x" << this->op << std::endl;
    std::cout << "base1: 0x" << this->base1 << std::endl;
    std::cout << "base2: 0x" << this->base2 << std::endl;
    std::cout << "base3: 0x" << this->base3 << std::endl;
    std::cout << "base4: 0x" << this->base4 << std::endl;
    std::cout << "baser: 0x" << this->baser << std::endl;
    std::cout << "size: 0x" << this->size << std::endl;
    std::cout << "arg1: 0x" << this->arg1 << std::endl;
    std::cout << "arg2: 0x" << this->arg2 << std::endl;
    std::cout << "arg3: 0x" << this->arg3 << std::dec << std::endl;
  }
};

} // namespace ramulator