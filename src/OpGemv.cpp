#include "OpGemv.h"
#include "ProcessingElement.h"
namespace ramulator {

OpGemv::OpGemv(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
               PimOpType type)
    : OpBase(p, memory, pe, type), num_column_(op_info_.arg1) {

  if (op_info_.mode & 0xf00)
    gemv_mode_ = GEMV_MODE::THREE_BANK;
  else
    gemv_mode_ = GEMV_MODE::TWO_BANK;

  if (gemv_mode_ == GEMV_MODE::THREE_BANK)
    last_atom_index_ =
        (op_info_.size / 3) * (op_info_.arg1 * elem_bytes_ / atom_bytes_);
  else if (gemv_mode_ == GEMV_MODE::TWO_BANK)
    last_atom_index_ =
        (op_info_.size / 2) * (op_info_.arg1 * elem_bytes_ / atom_bytes_);
  else
    assert(0);

  init_done();
  init_exec();
}

void OpGemv::init_done() {
  done_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ1);
  };
  done_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ2);
  };

  if (gemv_mode_ == GEMV_MODE::THREE_BANK) {
    done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) {
      return this->mem_done(STAGE::READ3);
    };
  } else if (gemv_mode_ == GEMV_MODE::TWO_BANK) {
    done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
  } else
    assert(0);

  done_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) {
    return this->mem_done(STAGE::READ4);
  };
  done_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
  done_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) {
    if ((idx + 1) % this->num_column() == 0) // end of each row
      return this->mem_done(STAGE::WRITE);

    return true;
  };
  done_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
}

void OpGemv::init_exec() {
  exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
    // Reading rows of matrix
    addr_t offset = idx * this->atom_bytes_;
    return this->issue_memop(this->base1() + offset, addr_mode_,
                             Request::Type::PIMREAD, STAGE::READ1);
  };
  exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) {
    // Reading rows of matrix
    addr_t offset = idx * this->atom_bytes_;
    return this->issue_memop(this->base2() + offset, addr_mode_,
                             Request::Type::PIMREAD, STAGE::READ2);
  };

  if (gemv_mode_ == GEMV_MODE::THREE_BANK) {
    exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) {
      // Reading rows of matrix
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->base3() + offset, addr_mode_,
                               Request::Type::PIMREAD, STAGE::READ3);
    };
  } else if (gemv_mode_ == GEMV_MODE::TWO_BANK) {
    exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
  } else
    assert(0);

  // Reading vector
  if (addr_mode_ == MODE::BLOCK) {
    exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) {
      addr_t offset = (idx % this->num_column()) * this->atom_bytes_;
      return this->issue_memop(this->base4() + offset, addr_mode_,
                               Request::Type::PIMREAD, STAGE::READ4);
    };
  } else {
    exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) {
      addr_t offset = idx * this->atom_bytes_;
      return this->issue_memop(this->base4() + offset, addr_mode_,
                               Request::Type::PIMREAD, STAGE::READ4);
    };
  }
  exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
  exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) {
    uint32_t num_column = this->num_column();
    if ((idx + 1) % num_column == 0) { // end of each row
      addr_t offset = (idx / num_column) * this->atom_bytes_;
      return this->issue_memop(this->baser() + offset, addr_mode_,
                               Request::Type::PIMWRITE, STAGE::WRITE);
    }

    return true;
  };
  exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
}

} // namespace ramulator