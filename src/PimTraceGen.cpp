#include "PimTraceGen.h"

namespace ramulator {

PimTraceGen::PimTraceGen(const char *pim_tracename)
    : trace_(pim_tracename), mode_(MODE::FILE_READ), kernel_() {}

PimTraceGen::PimTraceGen(const Config &cfg, const char *pim_tracename,
                         MemoryBase *memory)
    : trace_(), mode_(MODE::REAL_TIME), kernel_(cfg, pim_tracename, memory) {}

bool PimTraceGen::read_from_file(PimPacket &packet) {
  bool last = false;
  const uint32_t packet_sz = 11;

  for (uint32_t i = 0; i < packet_sz; i++) {
    PimPacket::Type type;
    long content;

    last = !trace_.get_pimtrace_request(content, type);

    if (last) {
      if (i != 0 && type != PimPacket::Type::PACKET_TAIL)
        std::cout << "[Error] Incomplete PIM packet" << std::endl;
      return true;
    }

    // Decode PIM command
    if (i == 0) {
      assert(type == PimPacket::Type::PACKET_HEAD);
      packet.mode = content;

    } else if (i == 1) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.op = content;

    } else if (i == 2) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.base1 = content;

    } else if (i == 3) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.base2 = content;

    } else if (i == 4) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.base3 = content;

    } else if (i == 5) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.base4 = content;

    } else if (i == 6) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.baser = content;

    } else if (i == 7) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.size = content;

    } else if (i == 8) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.arg1 = content;

    } else if (i == 9) {
      assert(type == PimPacket::Type::PACKET_BODY);
      packet.arg2 = content;

    } else if (i == 10) {
      assert(type == PimPacket::Type::PACKET_TAIL);
      packet.arg3 = content;
    }
  }

  return last;
}

inline bool PimTraceGen::generate_packet(PimPacket &packet) {
  return kernel_.get_next_packet(packet);
}

} // namespace ramulator
