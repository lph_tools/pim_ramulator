#include <iostream>

#include "Memory.h"
#include "OpBase.h"
#include "OpCopy.h"
#include "PimPacket.h"
#include "ProcessingElement.h"

class PimOpFactory {
 public:
  static OpBase* create_op(PimPacket& p, MemoryBase* memory, PimOpType type);
  enum class OPTYPE : uint32_t {
    AXPBY,
    AXPBYPCZ,
    AXPY,
    COPY,
    DOT,
    DOTC,
    GS,
    JACOBI,
    NRM2,
    SCAL,
    GEMV,
    XMY,
    SIZE
  };
};

OpBase* PimOpFactory::create_op(PimPacket& p, MemoryBase* memory,
                                ProcessingElement* pe PimOpType type) {
  // Special operation that notifies the end of execution
  if (type == PimOpType::DONE) return new OpDone(p, memory);

  switch (p.op) {
    case uint32_t(OPTYPE::AXPBY):
    case uint32_t(OPTYPE::AXPBYPCZ):
    case uint32_t(OPTYPE::AXPY):
    case uint32_t(OPTYPE::COPY):
      return new OpCopy(p, memory, pe);
      break;
    case uint32_t(OPTYPE::DOT):
    case uint32_t(OPTYPE::DOTC):
    case uint32_t(OPTYPE::GS):
    case uint32_t(OPTYPE::JACOBI):
    case uint32_t(OPTYPE::NRM2):
    case uint32_t(OPTYPE::SCAL):
    case uint32_t(OPTYPE::GEMV):
    case uint32_t(OPTYPE::XMY):
    default:
      std::cout << "[Error] Unknown PIM operation #" << p.op << std::endl;
      exit(EXIT_FAILURE);
      break;
  }
}