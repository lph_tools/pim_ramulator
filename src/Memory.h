#ifndef __MEMORY_H
#define __MEMORY_H

#include "Config.h"
#include "Controller.h"
#include "DDR4.h"
#include "DRAM.h"
#include "DSARP.h"
#include "GDDR5.h"
#include "HBM.h"
#include "LPDDR3.h"
#include "LPDDR4.h"
#include "PIM.h"
#include "Request.h"
#include "SpeedyController.h"
#include "Statistics.h"
#include "WideIO2.h"
#include <cassert>
#include <cmath>
#include <functional>
#include <tuple>
#include <vector>

using namespace std;

namespace ramulator {

class MemoryBase {
public:
  MemoryBase() {}
  virtual ~MemoryBase() {}
  virtual double clk_ns() = 0;
  virtual void tick() = 0;
  virtual bool send(Request req) = 0;
  virtual int pending_requests() = 0;
  virtual void finish(void) = 0;
  virtual long page_allocator(long addr, int coreid) = 0;
  virtual void record_core(int coreid) = 0;
  virtual void set_high_writeq_watermark(const float watermark) = 0;
  virtual void set_low_writeq_watermark(const float watermark) = 0;
  // for PIM
  virtual long calc_addr(long base_addr, long offset) const = 0;
  virtual uint32_t num_total_bank() const = 0;
  virtual long bank_capacity() const = 0;
  virtual long bank_addr(long index) const = 0;
  virtual uint32_t prefetch_size() const = 0;
  virtual uint32_t atom_bytes() const = 0;
  virtual addr_t add_to_row_addr(addr_t base, int32_t row_inc) const = 0;
  virtual addr_t add_to_bank_addr(addr_t base, addr_t bank_inc) const = 0;
  virtual long row_size() const = 0;
  virtual uint32_t local_bank_index(addr_t addr) const = 0;
};

template <class T, template <typename> class Controller = Controller>
class Memory : public MemoryBase {
protected:
  ScalarStat dram_capacity;
  ScalarStat num_dram_cycles;
  ScalarStat num_incoming_requests;
  VectorStat num_read_requests;
  VectorStat num_write_requests;
  ScalarStat ramulator_active_cycles;
  VectorStat incoming_requests_per_channel;
  VectorStat incoming_read_reqs_per_channel;

  ScalarStat physical_page_replacement;
  ScalarStat maximum_bandwidth;
  ScalarStat in_queue_req_num_sum;
  ScalarStat in_queue_read_req_num_sum;
  ScalarStat in_queue_write_req_num_sum;
  ScalarStat in_queue_req_num_avg;
  ScalarStat in_queue_read_req_num_avg;
  ScalarStat in_queue_write_req_num_avg;

#ifndef INTEGRATED_WITH_GEM5
  VectorStat record_read_requests;
  VectorStat record_write_requests;
#endif

  long max_address;

public:
  enum class Type {
    ChRaBaRoCo,
    RoBaRaCoCh,
    BLOCK_HOST, // BaBgRoPgRaCoCh
    INTLV_HOST, // RoBaBgPgRaCoCh
    BLOCK_PIM_FB,
    BLOCK_PIM_TBTSB,
    BLOCK_PIM_OBFSB,
    INTLV_PIM_FB,
    INTLV_PIM_TBTSB,
    INTLV_PIM_OBFSB,
    ChRoRaBaCo,
    ChRaRoBaCo,
    MAX
  } type,
      pim_addr_type;

  enum class DDR4_Type {
    ChRaRoCoBa,
    MAX
  } ddr4_addr_type;

  std::map<string, Type> mapping_type = {
      {"ChRaBaRoCo", Type::ChRaBaRoCo},           // Given 1
      {"RoBaRaCoCh", Type::RoBaRaCoCh},           // Given 2
      {"BLOCK_HOST", Type::BLOCK_HOST},           // BLOCK_HOST
      {"INTLV_HOST", Type::INTLV_HOST},           // INTLV_HOST
      {"BLOCK_PIM_FB", Type::BLOCK_PIM_FB},       // BLOCK_PIM_FB
      {"BLOCK_PIM_TBTSB", Type::BLOCK_PIM_TBTSB}, // BLOCK_PIM_TBTSB
      {"BLOCK_PIM_OBFSB", Type::BLOCK_PIM_OBFSB}, // BLOCK_PIM_OBFSB
      {"INTLV_PIM_FB", Type::INTLV_PIM_FB},       // INTLV_PIM_FB
      {"INTLV_PIM_TBTSB", Type::INTLV_PIM_TBTSB}, // INTLV_PIM_TBTSB
      {"INTLV_PIM_OBFSB", Type::INTLV_PIM_OBFSB},  // INTLV_PIM_OBFSB
      {"ChRoRaBaCo", Type::ChRoRaBaCo}, 
      {"ChRaRoBaCo", Type::ChRaRoBaCo}, 
  };

  std::map<string, Type> ddr4_mapping_type = {
    // {"ChRaRoCoBa", DDR4_Type::ChRaRoCoBa},
    // {"ChRoCoBaRa", DDR4_Type::ChRoCoBaRa},
    // {"ChRaBaCoRo", DDR4_Type::ChRaBaCoRo},
    // {"ChRaBaRoCo", DDR4_Type::ChRaBaRoCo},
    // {"ChRoCoRaBa", DDR4_Type::ChRoCoRaBa},
    // {"ChRoBaRaCo", DDR4_Type::ChRoBaRaCo},
    // {"RoCoRaBaCh", DDR4_Type::RoCoRaBaCh},
    // {"RoRaBaChCo", DDR4_Type::RoRaBaChCo}

  };

  enum class Translation {
    None,
    Random,
    MAX,
  } translation = Translation::None;

  std::map<string, Translation> name_to_translation = {
      {"None", Translation::None},
      {"Random", Translation::Random},
  };

  vector<int> free_physical_pages;
  long free_physical_pages_remaining;
  map<pair<int, long>, long> page_translation;

  vector<Controller<T> *> ctrls;
  T *spec;
  vector<int> addr_bits;

  int tx_bits;

  Memory(const Config &configs, vector<Controller<T> *> ctrls)
      : ctrls(ctrls), spec(ctrls[0]->channel->spec),
        addr_bits(int(T::Level::MAX)) {
    // make sure 2^N channels/ranks
    // TODO support channel number that is not powers of 2
    int *sz = spec->org_entry.count;
    assert((sz[0] & (sz[0] - 1)) == 0);
    assert((sz[1] & (sz[1] - 1)) == 0);
    // validate size of one transaction
    int tx = (spec->prefetch_size * spec->channel_width / 8);
    tx_bits = calc_log2(tx);
    assert((1 << tx_bits) == tx);

    if (configs.contains("host_address_mapping"))
      type = mapping_type[configs["host_address_mapping"]];
    else
      type = Type::ChRaBaRoCo; // default

    if (configs.contains("pim_address_mapping"))
      pim_addr_type = mapping_type[configs["pim_address_mapping"]];
    else
      pim_addr_type = Type::MAX;

    // If hi address bits will not be assigned to Rows
    // then the chips must not be LPDDRx 6Gb, 12Gb etc.
    if (type != Type::RoBaRaCoCh && spec->standard_name.substr(0, 5) == "LPDDR")
      assert((sz[int(T::Level::Row)] & (sz[int(T::Level::Row)] - 1)) == 0);

    max_address = spec->channel_width / 8;

    for (unsigned int lev = 0; lev < addr_bits.size(); lev++) {
      addr_bits[lev] = calc_log2(sz[lev]);
      max_address *= sz[lev];
    }

    addr_bits[int(T::Level::MAX) - 1] -= calc_log2(spec->prefetch_size);

    // Initiating translation
    if (configs.contains("translation")) {
      translation = name_to_translation[configs["translation"]];
    }
    if (translation != Translation::None) {
      // construct a list of available pages
      // TODO: this should not assume a 4KB page!
      free_physical_pages_remaining = max_address >> 12;

      free_physical_pages.resize(free_physical_pages_remaining, -1);
    }

    dram_capacity.name("dram_capacity")
        .desc("Number of bytes in simulated DRAM")
        .precision(0);
    dram_capacity = max_address;

    num_dram_cycles.name("dram_cycles")
        .desc("Number of DRAM cycles simulated")
        .precision(0);
    num_incoming_requests.name("incoming_requests")
        .desc("Number of incoming requests to DRAM")
        .precision(0);
    num_read_requests.init(configs.get_core_num())
        .name("read_requests")
        .desc("Number of incoming read requests to DRAM per core")
        .precision(0);
    num_write_requests.init(configs.get_core_num())
        .name("write_requests")
        .desc("Number of incoming write requests to DRAM per core")
        .precision(0);
    incoming_requests_per_channel.init(sz[int(T::Level::Channel)])
        .name("incoming_requests_per_channel")
        .desc("Number of incoming requests to each DRAM channel");
    incoming_read_reqs_per_channel.init(sz[int(T::Level::Channel)])
        .name("incoming_read_reqs_per_channel")
        .desc("Number of incoming read requests to each DRAM channel");

    ramulator_active_cycles.name("ramulator_active_cycles")
        .desc(
            "The total number of cycles that the DRAM part is active (serving "
            "R/W)")
        .precision(0);
    physical_page_replacement.name("physical_page_replacement")
        .desc("The number of times that physical page replacement happens.")
        .precision(0);
    maximum_bandwidth.name("maximum_bandwidth")
        .desc("The theoretical maximum bandwidth (Bps)")
        .precision(0);
    in_queue_req_num_sum.name("in_queue_req_num_sum")
        .desc("Sum of read/write queue length")
        .precision(0);
    in_queue_read_req_num_sum.name("in_queue_read_req_num_sum")
        .desc("Sum of read queue length")
        .precision(0);
    in_queue_write_req_num_sum.name("in_queue_write_req_num_sum")
        .desc("Sum of write queue length")
        .precision(0);
    in_queue_req_num_avg.name("in_queue_req_num_avg")
        .desc("Average of read/write queue length per memory cycle")
        .precision(6);
    in_queue_read_req_num_avg.name("in_queue_read_req_num_avg")
        .desc("Average of read queue length per memory cycle")
        .precision(6);
    in_queue_write_req_num_avg.name("in_queue_write_req_num_avg")
        .desc("Average of write queue length per memory cycle")
        .precision(6);
#ifndef INTEGRATED_WITH_GEM5
    record_read_requests.init(configs.get_core_num())
        .name("record_read_requests")
        .desc(
            "record read requests for this core when it reaches request limit "
            "or to the end");

    record_write_requests.init(configs.get_core_num())
        .name("record_write_requests")
        .desc(
            "record write requests for this core when it reaches request limit "
            "or to the end");
#endif
  }

  ~Memory() {
    for (auto ctrl : ctrls)
      delete ctrl;
    delete spec;
  }

  double clk_ns() { return spec->speed_entry.tCK; }

  void record_core(int coreid) {
#ifndef INTEGRATED_WITH_GEM5
    record_read_requests[coreid] = num_read_requests[coreid];
    record_write_requests[coreid] = num_write_requests[coreid];
#endif
    for (auto ctrl : ctrls) {
      ctrl->record_core(coreid);
    }
  }

  void tick() {
    ++num_dram_cycles;
    int cur_que_req_num = 0;
    int cur_que_readreq_num = 0;
    int cur_que_writereq_num = 0;
    for (auto ctrl : ctrls) {
      cur_que_req_num +=
          ctrl->readq.size() + ctrl->writeq.size() + ctrl->pending.size();
      cur_que_readreq_num += ctrl->readq.size() + ctrl->pending.size();
      cur_que_writereq_num += ctrl->writeq.size();
    }
    in_queue_req_num_sum += cur_que_req_num;
    in_queue_read_req_num_sum += cur_que_readreq_num;
    in_queue_write_req_num_sum += cur_que_writereq_num;

    bool is_active = false;
    for (auto ctrl : ctrls) {
      is_active = is_active || ctrl->is_active();
      ctrl->tick();
    }
    if (is_active) {
      ramulator_active_cycles++;
    }
  }

  bool send(Request req) {
    req.addr_vec.resize(addr_bits.size());
    long addr = req.addr;
    int coreid = req.coreid;

    // Each transaction size is 2^tx_bits, so first clear the lowest tx_bits
    // bits
    clear_lower_bits(addr, tx_bits);

    switch (int(type)) {
    case int(Type::ChRaBaRoCo):
      for (int i = addr_bits.size() - 1; i >= 0; i--)
        req.addr_vec[i] = slice_lower_bits(addr, addr_bits[i]);
      break;
    case int(Type::RoBaRaCoCh):
      req.addr_vec[0] = slice_lower_bits(addr, addr_bits[0]);
      req.addr_vec[addr_bits.size() - 1] =
          slice_lower_bits(addr, addr_bits[addr_bits.size() - 1]);
      for (int i = 1; i <= int(T::Level::Row); i++)
        req.addr_vec[i] = slice_lower_bits(addr, addr_bits[i]);
      break;
    default:
      assert(false);
    }

    if (ctrls[req.addr_vec[0]]->enqueue(req)) {
      // tally stats here to avoid double counting for requests that aren't
      // enqueued
      ++num_incoming_requests;
      if (req.type == Request::Type::READ) {
        ++num_read_requests[coreid];
        ++incoming_read_reqs_per_channel[req.addr_vec[int(T::Level::Channel)]];
      }
      if (req.type == Request::Type::WRITE) {
        ++num_write_requests[coreid];
      }
      ++incoming_requests_per_channel[req.addr_vec[int(T::Level::Channel)]];
      return true;
    }

    return false;
  }

  int pending_requests() {
    int reqs = 0;
    for (auto ctrl : ctrls)
      reqs += ctrl->readq.size() + ctrl->writeq.size() + ctrl->otherq.size() +
              ctrl->actq.size() + ctrl->pending.size() + ctrl->pimactq.size() +
              ctrl->pimq->pending_size();
    return reqs;
  }

  void set_high_writeq_watermark(const float watermark) {
    for (auto ctrl : ctrls)
      ctrl->set_high_writeq_watermark(watermark);
  }

  void set_low_writeq_watermark(const float watermark) {
    for (auto ctrl : ctrls)
      ctrl->set_low_writeq_watermark(watermark);
  }

  void finish(void) {
    dram_capacity = max_address;
    int *sz = spec->org_entry.count;
    maximum_bandwidth = spec->speed_entry.rate * 1e6 * spec->channel_width *
                        sz[int(T::Level::Channel)] / 8;
    long dram_cycles = num_dram_cycles.value();
    for (auto ctrl : ctrls) {
      long read_req =
          long(incoming_read_reqs_per_channel[ctrl->channel->id].value());
      ctrl->finish(read_req, dram_cycles);
    }

    // finalize average queueing requests
    in_queue_req_num_avg = in_queue_req_num_sum.value() / dram_cycles;
    in_queue_read_req_num_avg = in_queue_read_req_num_sum.value() / dram_cycles;
    in_queue_write_req_num_avg =
        in_queue_write_req_num_sum.value() / dram_cycles;
  }

  long page_allocator(long addr, int coreid) {
    long virtual_page_number = addr >> 12;

    switch (int(translation)) {
    case int(Translation::None): {
      return addr;
    }
    case int(Translation::Random): {
      auto target = make_pair(coreid, virtual_page_number);
      if (page_translation.find(target) == page_translation.end()) {
        // page doesn't exist, so assign a new page
        // make sure there are physical pages left to be assigned

        // if physical page doesn't remain, replace a previous assigned
        // physical page.
        if (!free_physical_pages_remaining) {
          physical_page_replacement++;
          long phys_page_to_read = lrand() % free_physical_pages.size();
          assert(free_physical_pages[phys_page_to_read] != -1);
          page_translation[target] = phys_page_to_read;
        } else {
          // assign a new page
          long phys_page_to_read = lrand() % free_physical_pages.size();
          // if the randomly-selected page was already assigned
          if (free_physical_pages[phys_page_to_read] != -1) {
            long starting_page_of_search = phys_page_to_read;

            do {
              // iterate through the list until we find a free page
              // TODO: does this introduce serious non-randomness?
              ++phys_page_to_read;
              phys_page_to_read %= free_physical_pages.size();
            } while ((phys_page_to_read != starting_page_of_search) &&
                     free_physical_pages[phys_page_to_read] != -1);
          }

          assert(free_physical_pages[phys_page_to_read] == -1);

          page_translation[target] = phys_page_to_read;
          free_physical_pages[phys_page_to_read] = coreid;
          --free_physical_pages_remaining;
        }
      }

      // SAUGATA TODO: page size should not always be fixed to 4KB
      return (page_translation[target] << 12) | (addr & ((1 << 12) - 1));
    }
    default:
      assert(false);
    }
  }

  // functions used for PIM
  unsigned int num_channel() const {
    return 1 << addr_bits[int(T::Level::Channel)];
  }

  unsigned int num_total_bank() const {
    int ch_idx = int(T::Level::Channel);
    int bk_idx = int(T::Level::Bank);
    assert(ch_idx < bk_idx);

    unsigned int shft_amt = 0;
    for (int i = ch_idx; i <= bk_idx; i++)
      shft_amt += addr_bits[i];

    return 1 << shft_amt;
  }

  unsigned int num_bank() const { return 1 << addr_bits[int(T::Level::Bank)]; }

  long bank_capacity() const { // in bytes
    unsigned int shft_amt = 0;
    for (int i = addr_bits.size() - 1; i >= int(T::Level::Bank); i--)
      shft_amt += addr_bits[i];

    long capacity = (1 << shft_amt) * spec->org_entry.dq / 8;

    return capacity;
  }

  long bank_addr(long bank_id) const {
    long curr_id = bank_id;
    std::vector<int> addr_vec(addr_bits.size(), 0);

    for (int i = int(T::Level::Bank); i >= int(T::Level::Channel); i--)
      addr_vec[i] = slice_lower_bits(curr_id, addr_bits[i]);

    long ret_addr = 0;
    switch (int(type)) {
    case int(Type::ChRaBaRoCo):
      for (int i = 0; i < addr_bits.size(); i++) {
        ret_addr <<= addr_bits[i];
        ret_addr |= addr_vec[i];
      }

      break;

    case int(Type::RoBaRaCoCh):
      for (int i = int(T::Level::Row); i >= 1; i--) {
        ret_addr <<= addr_bits[i];
        ret_addr |= addr_vec[i];
      }
      ret_addr <<= addr_bits[addr_bits.size() - 1];
      ret_addr |= addr_vec[addr_bits.size() - 1];
      ret_addr <<= addr_bits[0];
      ret_addr |= addr_vec[0];
      break;

    default:
      assert(false);
      break;
    }

    ret_addr *= (spec->org_entry.dq / 8);

    return ret_addr;
  }

  unsigned int addr_vec_size() const { return addr_bits.size(); }

  long calc_addr(long base_addr, long offset) const {
    return base_addr + offset; // TODO: change this later
  }

  uint32_t prefetch_size() const { return spec->prefetch_size; }
  uint32_t atom_bytes() const { return spec->org_entry.dq / 8; }

  std::vector<typename T::Level> get_levels(Type type) const {
    return vector<typename T::Level>();
  }

  long row_size() const {
    return atom_bytes() * (1 << addr_bits[int(T::Level::Column)]);
  }

  long add_to_row_addr(addr_t base, int32_t row_inc) const {
    std::vector<int> addr_vec(addr_bits.size(), 0);
    encode(addr_vec, base, pim_addr_type);

    addr_vec[int(T::Level::Row)] += row_inc;

    return decode(addr_vec, pim_addr_type);
  }

  long add_to_bank_addr(addr_t base, addr_t bank_inc) const {
    std::vector<int> addr_vec(addr_bits.size(), 0);
    encode(addr_vec, base, pim_addr_type);

    addr_vec[int(T::Level::Bank)] += bank_inc;

    return decode(addr_vec, pim_addr_type);
  }

  uint32_t local_bank_index(addr_t addr) const {
    assert(0); // should not call this
    return 0;
  }

  long decode(const std::vector<int> &addr_vec, Type type) const { 
    long ret_addr = 0l;

    vector<typename T::Level> levels = get_levels(type);
    for (int i = 0, size = levels.size(); i < size; ++i) {
      int level = int(levels[i]);
      ret_addr <<= int(addr_bits[level]);
      ret_addr |= addr_vec[level];
    }
    ret_addr <<= calc_log2(spec->org_entry.dq / 8);

    return ret_addr;
  }
  
  void encode(std::vector<int> &addr_vec, long addr, Type type) const {
    vector<typename T::Level> levels = get_levels(type);
    clear_lower_bits(addr, calc_log2(spec->channel_width / 8));

    for (int i = levels.size() - 1; i >= 0; --i) {
      int level = int(levels[i]);
      addr_vec[level] = slice_lower_bits(addr, addr_bits[level]);
    }
  }

private:
  int calc_log2(int val) const {
    int n = 0;
    while ((val >>= 1))
      n++;
    return n;
  }
  int slice_lower_bits(long &addr, int bits) const {
    int lbits = addr & ((1 << bits) - 1);
    addr >>= bits;
    return lbits;
  }
  void clear_lower_bits(long &addr, int bits) const { addr >>= bits; }
  long lrand(void) {
    if (sizeof(int) < sizeof(long)) {
      return static_cast<long>(rand()) << (sizeof(int) * 8) | rand();
    }

    return rand();
  }
};

template <> bool Memory<PIM>::send(Request req);
template <> long Memory<PIM>::add_to_bank_addr(long base, long bank_inc) const;
template <> uint32_t Memory<PIM>::local_bank_index(addr_t addr) const;

} /*namespace ramulator*/

#endif /*__MEMORY_H*/
