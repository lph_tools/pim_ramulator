#pragma once

#include "Allocator.h"
#include "Config.h"
#include "PimOp.h"
#include "PimPacket.h"
#include "common.h"
#include <iterator>
#include <sstream>
#include <string>
#include <unordered_map>

namespace ramulator {
class MemoryBase;

class PimKernel {
public:
  PimKernel() {}
  PimKernel(const Config &cfg, const char *pim_tracename, MemoryBase *memory)
      : trace_file_(pim_tracename), trace_file_name_(pim_tracename),
        line_number_(0), alloc_(cfg, memory) {

    assert(cfg.contains("addr_mode"));
    assert(cfg.contains("map_file"));
    assert(cfg.contains("auto_decomp"));

    if (cfg["addr_mode"] == "block")
      addr_mode_ = PIM_ADDR_MODE::BLOCK;
    else
      addr_mode_ = PIM_ADDR_MODE::INTLV;

    std::string map_file = cfg["map_file"];
    auto_decomp_ = std::stol(cfg["auto_decomp"]);

    init_var_table(map_file);
  }
  bool get_next_packet(PimPacket &packet);

private:
  void init_var_table(const std::string &map_file) {
    std::ifstream map_stream(map_file);
    std::string line;

    if (!map_stream.good()) {
      std::cerr << "Bad map file: " << map_file << std::endl;
      exit(EXIT_FAILURE);
    }

    uint32_t line_num = 1;

    while (!map_stream.eof()) {
      std::getline(map_stream, line);
      if (line.empty())
        continue;
      std::istringstream iss(line);
      std::string key, num_row, num_col, base_bank;

      if (num_words(line) != 4) {
        std::cerr << "[Error] Wrong mapping format at line " << line_num
                  << " in " << map_file << std::endl;
        exit(EXIT_FAILURE);
      }

      iss >> key;
      iss >> num_row;
      iss >> num_col;
      iss >> base_bank;

      uint32_t n_col = std::stol(num_col);
      // uint32_t n_row = std::stol(num_row) / alloc_.tot_num_pe();
      uint32_t n_row = std::stol(num_row);
      uint32_t base_bk = std::stoi(base_bank);
      const uint32_t elem_bytes = 4; // FIXME: hardcoded to single precision

      if (auto_decomp_)
        n_row /= alloc_.tot_num_pe();

      if (n_col == 1) { // vector
        std::vector<addr_t> base_addrs;
        alloc_.allocate_vector(base_bk, base_addrs, n_row * elem_bytes);
        assert(var_table_.find(key) == var_table_.end());
        var_table_[key] = VarInfo{base_bk, -1, n_row, n_col, base_addrs};

      } else { // matrix
        addr_t base = alloc_.allocate(base_bk, n_col * n_row * elem_bytes);
        if (var_table_.find(key) == var_table_.end())
          var_table_[key] = VarInfo{base_bk, base, n_row, n_col};
        else {
          if (var_table_[key].base_addr_vec.size() == 0)
            var_table_[key].base_addr_vec.push_back(var_table_[key].base_addr);
          var_table_[key].base_addr_vec.push_back(base);
          var_table_[key].num_row += n_row;
        }
      }

      ++line_num;
    }

    print_var_table();
  }

  struct VarInfo {
    uint32_t base_bank;
    addr_t base_addr;
    addr_t num_row;
    addr_t num_col;
    std::vector<addr_t> base_addr_vec;
  };

  uint32_t num_words(std::string const &str) {
    std::stringstream stream(str);
    return std::distance(std::istream_iterator<std::string>(stream),
                         std::istream_iterator<std::string>());
  }

  void print_var_table() {
    for (auto const &var : var_table_) {
      std::string var_name = var.first;
      addr_t size = var.second.num_row * var.second.num_col;
      std::cout << "[" << var_name << "] size = " << size << " elements @ ";
      if (var.second.base_addr_vec.size() > 0) {
        for (auto const &addr : var.second.base_addr_vec)
          std::cout << std::hex << "0x" << addr << " ";
        std::cout << std::dec << std::endl;
      } else {
        std::cout << std::hex << "0x" << var.second.base_addr << std::dec
                  << std::endl;
      }
    }
  }

  PIM_ADDR_MODE addr_mode_;
  std::unordered_map<std::string, VarInfo> var_table_;
  std::ifstream trace_file_;
  std::string trace_file_name_;
  uint32_t line_number_;
  Allocator alloc_;
  bool auto_decomp_;
};

} // namespace ramulator
