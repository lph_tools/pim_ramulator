#include "PimInstructionGenerator.h"
#include "PimInstruction.h"
#include "PimOp.h"

namespace ramulator {

PimInstructionGenerator::PimInstructionGenerator(MemoryBase *memory,
                                                 ProcessingElement *pe)
    : memory_(memory), front_inst_(nullptr), pe_(pe), op_(nullptr), index_(0) {
  PimPacket packet;
  done_op_ = new PimOp(packet, memory_, pe_, PimOpType::DONE);
}

PimInstructionGenerator::~PimInstructionGenerator() {
  for (auto it = inst_pool_.begin(); it != inst_pool_.end(); ++it)
    delete *it;
  if (op_)
    delete op_;
  delete done_op_;
}

void PimInstructionGenerator::set_operation(PimPacket &p) {
  // delete previous op
  if (op_)
    delete op_;

  op_ = new PimOp(p, memory_, pe_);
  index_ = 0;
}

void PimInstructionGenerator::push_for_reuse(PimInstruction *p) {
  inst_pool_.push_back(p);
}

PimInstruction *PimInstructionGenerator::front() {
  PimOp *op;
  if (done())
    op = done_op_;
  else
    op = op_;

  if (!front_inst_) {
    if (inst_pool_.size() == 0) {
      front_inst_ = new PimInstruction(this, op, index_);
    } else {
      front_inst_ = inst_pool_.front();
      inst_pool_.pop_front();
      front_inst_->setup(this, op, index_, InstState::READY);
    }
  }

  return front_inst_;
}

void PimInstructionGenerator::pop() {
  front_inst_ = nullptr;
  index_++;
}

bool PimInstructionGenerator::done() {
  assert(op_);
  return op_->reached_end(index_);
}

} // namespace ramulator
