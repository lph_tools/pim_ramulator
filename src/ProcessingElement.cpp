#include <cassert>

#include "ProcessingElement.h"

namespace ramulator {

ProcessingElement::ProcessingElement(const Config &config, MemoryBase *memory,
                                     const PeConfig &pe_cfg,
                                     PimController *pim_ctrl)
    : base_addr_(pe_cfg.base), size_(pe_cfg.size), id_(pe_cfg.id),
      num_local_sb_(pe_cfg.num_sb), done_(true), pim_inst_gen_(memory, this),
      pim_ctrl_(pim_ctrl) {
  pipeline_.reserve(7);

  // placement new (for locality)
  pipeline_.emplace_back(STAGE::READ1, STAGE::READ1, &pipeline_[1], 1u);
  pipeline_.emplace_back(STAGE::READ2, STAGE::READ1, &pipeline_[2], 1u);
  pipeline_.emplace_back(STAGE::READ3, STAGE::READ2, &pipeline_[3], 1u);
  pipeline_.emplace_back(STAGE::READ4, STAGE::READ3, &pipeline_[4], 1u);
  pipeline_.emplace_back(STAGE::COMPT, STAGE::READ4, &pipeline_[5], 1u);
  pipeline_.emplace_back(STAGE::WRITE, STAGE::COMPT, &pipeline_[6], 1u);
  pipeline_.emplace_back(STAGE::COMMT, STAGE::WRITE, nullptr, 1u);
}

ProcessingElement::~ProcessingElement() {
  // for (auto it = pipeline_.begin(); it != pipeline_.end(); ++it)
  //   delete (*it);
}

void ProcessingElement::launch(PimPacket &p) {
  pim_inst_gen_.set_operation(p);
  done_ = false;
  reset();
}

void ProcessingElement::reset() {
  assert(pipeline_.size() != 0);

  for (auto it = pipeline_.begin(); it != pipeline_.end(); ++it)
    it->reset();
}

bool ProcessingElement::tick() {
  if (done_)
    return true;

#ifdef DEBUG
  std::cout << std::endl << "PE" << id_ << ", Cycle: " << ++clk_ << std::endl;
#endif

  PimInstruction *p = pim_inst_gen_.front();

  if (pipeline_[0].add_instruction(p))
    pim_inst_gen_.pop();

  bool last_reached = false;
  for (auto it = pipeline_.rbegin(); it != pipeline_.rend(); ++it)
    last_reached |= it->tick();

  if (last_reached)
    done_ = true;

  return done_;
}

addr_t ProcessingElement::size() { return size_; }

uint32_t ProcessingElement::num_local_sb() { return num_local_sb_; }

bool ProcessingElement::done() { return done_; }

addr_t ProcessingElement::base_addr() { return base_addr_; }

void ProcessingElement::update_next() {
  uint32_t sz = pipeline_.size();
  for (uint32_t i = 0; i < sz - 1; ++i)
    pipeline_[i].set_next(&pipeline_[i + 1]);
}

uint32_t ProcessingElement::id() { return id_; }

PimController *ProcessingElement::pim_ctrl() { return pim_ctrl_; }

} // namespace ramulator