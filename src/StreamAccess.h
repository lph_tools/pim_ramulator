#pragma once

#include "common.h"
#include <cstdlib>
#include <iostream>

namespace ramulator {

class StreamAccess {
public:
  StreamAccess() : addr_(0), end_(0), stride_(0) {}
  StreamAccess(addr_t start, addr_t end, uint32_t stride)
      : addr_(start), end_(end), stride_(stride) {}

  addr_t next_addr() {
    addr_t next = addr_;
    addr_ += stride_;
    return next;
  }
  bool no_more_addr() const { return addr_ > end_; }

private:
  addr_t addr_;
  addr_t end_;
  uint32_t stride_;
};
} // namespace ramulator