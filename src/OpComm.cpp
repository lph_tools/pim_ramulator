#include "OpComm.h"
#include "PimController.h"
#include "ProcessingElement.h"

namespace ramulator {

OpComm::OpComm(PimPacket &p, MemoryBase *memory, ProcessingElement *pe,
               PimOpType type)
    : OpBase(p, memory, pe, type), multiple_(op_info_.mode >> 8 & 0xf),
      distance_(multiple_ / 2) {
  init_done();
  init_exec();

  if (pe_->id() % multiple_ != 0)
    last_atom_index_ = 0;
  if (addr_mode_ == MODE::BLOCK)
    last_atom_index_ = op_info_.size * elem_bytes_ / atom_bytes_;
  else if (addr_mode_ == MODE::INTLV)
    last_atom_index_ = op_info_.size * elem_bytes_ / atom_bytes_ / 4;

  if (pe_->id() % multiple_ != 0) {
    src_pim_id_ = (uint32_t)-1;
    src_base_ = (addr_t)-1;
    dst_base_ = (addr_t)-1;
  } else {
    src_pim_id_ = pe->id() + distance_;
    src_base_ = pe->pim_ctrl()->base_addr(src_pim_id_);
    dst_base_ = pe->base_addr();
  }
}

void OpComm::init_done() {
  if (pe_->id() % multiple_ == 0) {
    done_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
      return this->mem_done(STAGE::READ1);
    };

    done_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };

    done_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) {
      return this->mem_done(STAGE::WRITE);
    };
    done_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };

  } else { // no operation
    done_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
    done_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
  }
}

void OpComm::init_exec() {
  if (pe_->id() % multiple_ == 0) {
    if (addr_mode_ == MODE::BLOCK) {
      exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
        addr_t offset = idx * this->atom_bytes_;
        return this->issue_memop(this->src_base() + op_info_.base1 + offset,
                                 MODE::BLOCK, Request::Type::PIMREAD,
                                 STAGE::READ1);
      };

      exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) {
        return true;
      };
      exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) {
        return true;
      };
      exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) {
        return true;
      };
      exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) {
        return true;
      };

      exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) {
        addr_t offset = idx * this->atom_bytes_;
        return this->issue_memop(this->dst_base() + op_info_.baser + offset,
                                 MODE::BLOCK, Request::Type::PIMWRITE,
                                 STAGE::WRITE);
      };
      exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) {
        return true;
      };

    } else if (addr_mode_ == MODE::INTLV) {
      exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) {
        addr_t offset = idx * this->atom_bytes_;
        bool issued = true;
        issued &= this->issue_memop(this->src_base() + op_info_.base1 + offset,
                                    MODE::BLOCK, Request::Type::PIMREAD,
                                    STAGE::READ1);
        issued &= this->issue_memop(this->src_base() + op_info_.base2 + offset,
                                    MODE::BLOCK, Request::Type::PIMREAD,
                                    STAGE::READ1);
        assert(issued);
        return true;
      };

      exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) {
        return true;
      };
      exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) {
        return true;
      };
      exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) {
        return true;
      };
      exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) {
        return true;
      };

      exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) {
        addr_t offset = idx * this->atom_bytes_;
        bool issued = true;
        issued &= this->issue_memop(this->dst_base() + op_info_.baser + offset,
                                    MODE::BLOCK, Request::Type::PIMWRITE,
                                    STAGE::WRITE);
        issued &= this->issue_memop(this->dst_base() + op_info_.base3 + offset,
                                    MODE::BLOCK, Request::Type::PIMWRITE,
                                    STAGE::WRITE);
        assert(issued);
        return true;
      };
      exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) {
        return true;
      };

    } else
      assert(0);

  } else { // no operation
    exec_func_[uint32_t(STAGE::READ1)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ2)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ3)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::READ4)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMPT)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::WRITE)] = [this](uint32_t idx) { return true; };
    exec_func_[uint32_t(STAGE::COMMT)] = [this](uint32_t idx) { return true; };
  }
}

addr_t OpComm::src_base() { return src_base_; }
addr_t OpComm::dst_base() { return dst_base_; }

} // namespace ramulator