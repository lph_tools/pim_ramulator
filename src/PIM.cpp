#include "PIM.h"
#include "DRAM.h"

#include <cassert>
#include <functional>
#include <vector>

using namespace std;
using namespace ramulator;

string PIM::standard_name = "PIM";

map<string, enum PIM::Org> PIM::org_map = {
    {"HBM_1Gb", PIM::Org::HBM_1Gb},
    {"HBM_2Gb", PIM::Org::HBM_2Gb},
    {"HBM_4Gb", PIM::Org::HBM_4Gb},
    {"HBM_2Gb_NOSB", PIM::Org::HBM_2Gb_NOSB},
    {"HBM_2Gb_TSB", PIM::Org::HBM_2Gb_TSB},
    {"HBM_2Gb_FSB", PIM::Org::HBM_2Gb_FSB},
};

map<string, enum PIM::Speed> PIM::speed_map = {
    {"HBM_1Gbps", PIM::Speed::HBM_1Gbps},
};

PIM::PIM(Org org, Speed speed, int num_local_bank)
    : org_entry(org_table[int(org)]), channel_width(org_table[int(org)].dq),
      speed_entry(speed_table[int(speed)]),
      read_latency(speed_entry.nCL + speed_entry.nBL),
      num_local_bank_(num_local_bank) {
  init_speed();
  init_prereq();
  init_rowhit(); // SAUGATA: added row hit function
  init_rowopen();
  init_lambda();
  init_timing();
}

PIM::PIM(const string &org_str, const string &speed_str, int num_local_bank)
    : PIM(org_map[org_str], speed_map[speed_str], num_local_bank) {}

void PIM::set_channel_number(int channel) {
  org_entry.count[int(Level::Channel)] = channel;
}

void PIM::set_rank_number(int rank) {
  org_entry.count[int(Level::Rank)] = rank;
}

void PIM::init_speed() {
  const static int RFC_TABLE[int(Speed::MAX)][int(Org::MAX)] = {{55, 80, 130}};
  const static int REFI1B_TABLE[int(Speed::MAX)][int(Org::MAX)] = {
      {64, 128, 256}};
  const static int XS_TABLE[int(Speed::MAX)][int(Org::MAX)] = {{60, 85, 135}};

  int speed = 0, density = 0;
  switch (speed_entry.rate) {
  case 1000:
    speed = 0;
    break;
  default:
    assert(false);
  };
  switch (org_entry.size >> 10) {
  case 1:
    density = 0;
    break;
  case 2:
    density = 1;
    break;
  case 4:
    density = 2;
    break;
  default:
    assert(false);
  }
  speed_entry.nRFC = RFC_TABLE[speed][density];
  speed_entry.nREFI1B = REFI1B_TABLE[speed][density];
  speed_entry.nXS = XS_TABLE[speed][density];
}

void PIM::init_prereq() {
  // RD
  prereq[int(Level::Rank)][int(Command::RD)] = [](DRAM<PIM> *node, Command cmd,
                                                  int id) {
    switch (int(node->state)) {
    case int(State::PowerUp):
      return Command::MAX;
    case int(State::ActPowerDown):
      return Command::PDX;
    case int(State::PrePowerDown):
      return Command::PDX;
    case int(State::SelfRefresh):
      return Command::SRX;
    default:
      assert(false);
    }
  };
  prereq[int(Level::SubBank)][int(Command::RD)] = [](DRAM<PIM> *node,
                                                     Command cmd, int id) {
    switch (int(node->state)) {
    case int(State::Closed):
      return Command::ACT;
    case int(State::Opened):
      if (node->row_state.find(id) != node->row_state.end())
        return cmd;
      else
        return Command::PRE;
    default:
      assert(false);
    }
  };

  // WR
  prereq[int(Level::Rank)][int(Command::WR)] =
      prereq[int(Level::Rank)][int(Command::RD)];
  prereq[int(Level::SubBank)][int(Command::WR)] =
      prereq[int(Level::SubBank)][int(Command::RD)];

  // PIMRD
  prereq[int(Level::Rank)][int(Command::PIMRD)] =
      prereq[int(Level::Rank)][int(Command::RD)];
  prereq[int(Level::SubBank)][int(Command::PIMRD)] =
      prereq[int(Level::SubBank)][int(Command::RD)];

  // PIMWR
  prereq[int(Level::Rank)][int(Command::PIMWR)] =
      prereq[int(Level::Rank)][int(Command::RD)];
  prereq[int(Level::SubBank)][int(Command::PIMWR)] =
      prereq[int(Level::SubBank)][int(Command::RD)];

  // REF
  prereq[int(Level::Rank)][int(Command::REF)] = [](DRAM<PIM> *node, Command cmd,
                                                   int id) {
    for (auto bg : node->children)
      for (auto bank : bg->children) {
        if (bank->state == State::Closed)
          continue;
        return Command::PREA;
      }
    return Command::REF;
  };

  // REFSB
  prereq[int(Level::Bank)][int(Command::REFSB)] = [](DRAM<PIM> *node,
                                                     Command cmd, int id) {
    if (node->state == State::Closed)
      return Command::REFSB;
    return Command::PRE;
  };

  // PD
  prereq[int(Level::Rank)][int(Command::PDE)] = [](DRAM<PIM> *node, Command cmd,
                                                   int id) {
    switch (int(node->state)) {
    case int(State::PowerUp):
      return Command::PDE;
    case int(State::ActPowerDown):
      return Command::PDE;
    case int(State::PrePowerDown):
      return Command::PDE;
    case int(State::SelfRefresh):
      return Command::SRX;
    default:
      assert(false);
    }
  };

  // SR
  prereq[int(Level::Rank)][int(Command::SRE)] = [](DRAM<PIM> *node, Command cmd,
                                                   int id) {
    switch (int(node->state)) {
    case int(State::PowerUp):
      return Command::SRE;
    case int(State::ActPowerDown):
      return Command::PDX;
    case int(State::PrePowerDown):
      return Command::PDX;
    case int(State::SelfRefresh):
      return Command::SRE;
    default:
      assert(false);
    }
  };
}

// SAUGATA: added row hit check functions to see if the desired location is
// currently open
void PIM::init_rowhit() {
  // RD
  rowhit[int(Level::SubBank)][int(Command::RD)] = [](DRAM<PIM> *node,
                                                     Command cmd, int id) {
    switch (int(node->state)) {
    case int(State::Closed):
      return false;
    case int(State::Opened):
      if (node->row_state.find(id) != node->row_state.end())
        return true;
      return false;
    default:
      assert(false);
    }
  };

  // WR
  rowhit[int(Level::SubBank)][int(Command::WR)] =
      rowhit[int(Level::SubBank)][int(Command::RD)];

  // PIMRD
  rowhit[int(Level::SubBank)][int(Command::PIMRD)] =
      rowhit[int(Level::SubBank)][int(Command::RD)];

  // PIMWR
  rowhit[int(Level::SubBank)][int(Command::PIMWR)] =
      rowhit[int(Level::SubBank)][int(Command::RD)];
}

void PIM::init_rowopen() {
  // RD
  rowopen[int(Level::SubBank)][int(Command::RD)] = [](DRAM<PIM> *node,
                                                      Command cmd, int id) {
    switch (int(node->state)) {
    case int(State::Closed):
      return false;
    case int(State::Opened):
      return true;
    default:
      assert(false);
    }
  };

  // WR
  rowopen[int(Level::SubBank)][int(Command::WR)] =
      rowopen[int(Level::SubBank)][int(Command::RD)];

  // PIMRD
  rowopen[int(Level::SubBank)][int(Command::PIMRD)] =
      rowopen[int(Level::SubBank)][int(Command::RD)];

  // PIMWR
  rowopen[int(Level::SubBank)][int(Command::PIMWR)] =
      rowopen[int(Level::SubBank)][int(Command::RD)];
}

void PIM::init_lambda() {
  lambda[int(Level::SubBank)][int(Command::ACT)] = [](DRAM<PIM> *node, int id) {
    node->state = State::Opened;
    node->row_state[id] = State::Opened;
  };
  lambda[int(Level::SubBank)][int(Command::PRE)] = [](DRAM<PIM> *node, int id) {
    node->state = State::Closed;
    node->row_state.clear();
  };
  lambda[int(Level::Rank)][int(Command::PREA)] = [](DRAM<PIM> *node, int id) {
    for (auto bg : node->children)
      for (auto bank : bg->children) {
        bank->state = State::Closed;
        bank->row_state.clear();
      }
  };
  lambda[int(Level::Rank)][int(Command::REF)] = [](DRAM<PIM> *node, int id) {};
  lambda[int(Level::SubBank)][int(Command::RD)] = [](DRAM<PIM> *node, int id) {
  };
  lambda[int(Level::SubBank)][int(Command::WR)] = [](DRAM<PIM> *node, int id) {
  };
  lambda[int(Level::SubBank)][int(Command::PIMRD)] = [](DRAM<PIM> *node,
                                                        int id) {};
  lambda[int(Level::SubBank)][int(Command::PIMWR)] = [](DRAM<PIM> *node,
                                                        int id) {};
  lambda[int(Level::SubBank)][int(Command::RDA)] = [](DRAM<PIM> *node, int id) {
    node->state = State::Closed;
    node->row_state.clear();
  };
  lambda[int(Level::SubBank)][int(Command::WRA)] = [](DRAM<PIM> *node, int id) {
    node->state = State::Closed;
    node->row_state.clear();
  };
  lambda[int(Level::Rank)][int(Command::PDE)] = [](DRAM<PIM> *node, int id) {
    for (auto bg : node->children)
      for (auto bank : bg->children) {
        if (bank->state == State::Closed)
          continue;
        node->state = State::ActPowerDown;
        return;
      }
    node->state = State::PrePowerDown;
  };
  lambda[int(Level::Rank)][int(Command::PDX)] = [](DRAM<PIM> *node, int id) {
    node->state = State::PowerUp;
  };
  lambda[int(Level::Rank)][int(Command::SRE)] = [](DRAM<PIM> *node, int id) {
    node->state = State::SelfRefresh;
  };
  lambda[int(Level::Rank)][int(Command::SRX)] = [](DRAM<PIM> *node, int id) {
    node->state = State::PowerUp;
  };
}

void PIM::init_timing() {
  SpeedEntry &s = speed_entry;
  vector<TimingEntry> *t;

  /*** Channel ***/
  t = timing[int(Level::Channel)];

  // CAS <-> CAS
  t[int(Command::RD)].push_back({Command::RD, 1, s.nBL});
  t[int(Command::RD)].push_back({Command::RDA, 1, s.nBL});
  t[int(Command::RD)].push_back({Command::PIMRD, 1, s.nBL});
  t[int(Command::RDA)].push_back({Command::RD, 1, s.nBL});
  t[int(Command::RDA)].push_back({Command::RDA, 1, s.nBL});
  t[int(Command::RDA)].push_back({Command::PIMRD, 1, s.nBL});
  t[int(Command::WR)].push_back({Command::WR, 1, s.nBL});
  t[int(Command::WR)].push_back({Command::WRA, 1, s.nBL});
  t[int(Command::WR)].push_back({Command::PIMWR, 1, s.nBL});
  t[int(Command::WRA)].push_back({Command::WR, 1, s.nBL});
  t[int(Command::WRA)].push_back({Command::WRA, 1, s.nBL});
  t[int(Command::WRA)].push_back({Command::PIMWR, 1, s.nBL});

  /*** Rank ***/
  t = timing[int(Level::Rank)];

  // CAS <-> CAS
  t[int(Command::RD)].push_back({Command::RD, 1, s.nCCDS});
  t[int(Command::RD)].push_back({Command::RDA, 1, s.nCCDS});
  t[int(Command::RD)].push_back({Command::PIMRD, 1, s.nCCDS});
  t[int(Command::RDA)].push_back({Command::RD, 1, s.nCCDS});
  t[int(Command::RDA)].push_back({Command::RDA, 1, s.nCCDS});
  t[int(Command::RDA)].push_back({Command::PIMRD, 1, s.nCCDS});
  t[int(Command::WR)].push_back({Command::WR, 1, s.nCCDS});
  t[int(Command::WR)].push_back({Command::WRA, 1, s.nCCDS});
  t[int(Command::WR)].push_back({Command::PIMWR, 1, s.nCCDS});
  t[int(Command::WRA)].push_back({Command::WR, 1, s.nCCDS});
  t[int(Command::WRA)].push_back({Command::WRA, 1, s.nCCDS});
  t[int(Command::WRA)].push_back({Command::PIMWR, 1, s.nCCDS});
  t[int(Command::RD)].push_back({Command::WR, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
  t[int(Command::RD)].push_back(
      {Command::WRA, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
  t[int(Command::RD)].push_back(
      {Command::PIMWR, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
  t[int(Command::RDA)].push_back(
      {Command::WR, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
  t[int(Command::RDA)].push_back(
      {Command::WRA, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
  t[int(Command::RDA)].push_back(
      {Command::PIMWR, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
  t[int(Command::WR)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRS});
  t[int(Command::WR)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRS});
  t[int(Command::WR)].push_back({Command::PIMRD, 1, s.nCWL + s.nBL + s.nWTRS});
  t[int(Command::WRA)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRS});
  t[int(Command::WRA)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRS});
  t[int(Command::WRA)].push_back({Command::PIMRD, 1, s.nCWL + s.nBL + s.nWTRS});

  t[int(Command::RD)].push_back({Command::PREA, 1, s.nRTP});
  t[int(Command::WR)].push_back({Command::PREA, 1, s.nCWL + s.nBL + s.nWR});
  t[int(Command::PIMRD)].push_back({Command::PREA, 1, s.nRTP});
  t[int(Command::PIMWR)].push_back({Command::PREA, 1, s.nCWL + s.nBL + s.nWR});

  // CAS <-> PD
  t[int(Command::RD)].push_back({Command::PDE, 1, s.nCL + s.nBL + 1});
  t[int(Command::RDA)].push_back({Command::PDE, 1, s.nCL + s.nBL + 1});
  t[int(Command::PIMRD)].push_back({Command::PDE, 1, s.nCL + s.nBL + 1});
  t[int(Command::WR)].push_back({Command::PDE, 1, s.nCWL + s.nBL + s.nWR});
  t[int(Command::WRA)].push_back(
      {Command::PDE, 1, s.nCWL + s.nBL + s.nWR + 1}); // +1 for pre
  t[int(Command::PIMWR)].push_back({Command::PDE, 1, s.nCWL + s.nBL + s.nWR});
  t[int(Command::PDX)].push_back({Command::RD, 1, s.nXP});
  t[int(Command::PDX)].push_back({Command::RDA, 1, s.nXP});
  t[int(Command::PDX)].push_back({Command::PIMRD, 1, s.nXP});
  t[int(Command::PDX)].push_back({Command::WR, 1, s.nXP});
  t[int(Command::PDX)].push_back({Command::WRA, 1, s.nXP});
  t[int(Command::PDX)].push_back({Command::PIMWR, 1, s.nXP});

  // CAS <-> SR: none (all banks have to be precharged)

  // RAS <-> RAS
  int num_sb = org_entry.count[int(Level::SubBank)];
  t[int(Command::ACT)].push_back({Command::ACT, num_sb, s.nRRDS});
  t[int(Command::ACT)].push_back({Command::ACT, 4 * num_sb, s.nFAW});
  t[int(Command::ACT)].push_back({Command::PREA, 1, s.nRAS});
  t[int(Command::PREA)].push_back({Command::ACT, 1, s.nRP});

  // RAS <-> REF
  t[int(Command::PRE)].push_back({Command::REF, 1, s.nRP});
  t[int(Command::PREA)].push_back({Command::REF, 1, s.nRP});
  t[int(Command::REF)].push_back({Command::ACT, 1, s.nRFC});

  // RAS <-> PD
  t[int(Command::ACT)].push_back({Command::PDE, 1, 1});
  t[int(Command::PDX)].push_back({Command::ACT, 1, s.nXP});
  t[int(Command::PDX)].push_back({Command::PRE, 1, s.nXP});
  t[int(Command::PDX)].push_back({Command::PREA, 1, s.nXP});

  // RAS <-> SR
  t[int(Command::PRE)].push_back({Command::SRE, 1, s.nRP});
  t[int(Command::PREA)].push_back({Command::SRE, 1, s.nRP});
  t[int(Command::SRX)].push_back({Command::ACT, 1, s.nXS});

  // REF <-> REF
  t[int(Command::REF)].push_back({Command::REF, 1, s.nRFC});

  // REF <-> PD
  t[int(Command::REF)].push_back({Command::PDE, 1, 1});
  t[int(Command::PDX)].push_back({Command::REF, 1, s.nXP});

  // REF <-> SR
  t[int(Command::SRX)].push_back({Command::REF, 1, s.nXS});

  // PD <-> PD
  t[int(Command::PDE)].push_back({Command::PDX, 1, s.nPD});
  t[int(Command::PDX)].push_back({Command::PDE, 1, s.nXP});

  // PD <-> SR
  t[int(Command::PDX)].push_back({Command::SRE, 1, s.nXP});
  t[int(Command::SRX)].push_back({Command::PDE, 1, s.nXS});

  // SR <-> SR
  t[int(Command::SRE)].push_back({Command::SRX, 1, s.nCKESR});
  t[int(Command::SRX)].push_back({Command::SRE, 1, s.nXS});

  /*** Bank Group ***/
  t = timing[int(Level::BankGroup)];
  // CAS <-> CAS
  t[int(Command::RD)].push_back({Command::RD, 1, s.nCCDL});
  t[int(Command::RD)].push_back({Command::RDA, 1, s.nCCDL});
  t[int(Command::RD)].push_back({Command::PIMRD, 1, s.nCCDL});
  t[int(Command::RDA)].push_back({Command::RD, 1, s.nCCDL});
  t[int(Command::RDA)].push_back({Command::RDA, 1, s.nCCDL});
  t[int(Command::RDA)].push_back({Command::PIMRD, 1, s.nCCDL});
  t[int(Command::WR)].push_back({Command::WR, 1, s.nCCDL});
  t[int(Command::WR)].push_back({Command::WRA, 1, s.nCCDL});
  t[int(Command::WR)].push_back({Command::PIMWR, 1, s.nCCDL});
  t[int(Command::WRA)].push_back({Command::WR, 1, s.nCCDL});
  t[int(Command::WRA)].push_back({Command::WRA, 1, s.nCCDL});
  t[int(Command::WRA)].push_back({Command::PIMWR, 1, s.nCCDL});
  t[int(Command::RD)].push_back({Command::WR, 1, s.nCCDL});
  t[int(Command::RD)].push_back({Command::WRA, 1, s.nCCDL});
  t[int(Command::RD)].push_back({Command::PIMWR, 1, s.nCCDL});
  t[int(Command::RDA)].push_back({Command::WR, 1, s.nCCDL});
  t[int(Command::RDA)].push_back({Command::WRA, 1, s.nCCDL});
  t[int(Command::RDA)].push_back({Command::PIMWR, 1, s.nCCDL});
  t[int(Command::WR)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRL});
  t[int(Command::WR)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRL});
  t[int(Command::WR)].push_back({Command::PIMRD, 1, s.nCWL + s.nBL + s.nWTRL});
  t[int(Command::WRA)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRL});
  t[int(Command::WRA)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRL});
  t[int(Command::WRA)].push_back({Command::PIMRD, 1, s.nCWL + s.nBL + s.nWTRL});

  // RAS <-> RAS
  t[int(Command::ACT)].push_back({Command::ACT, num_sb, s.nRRDL});

  /*** SubBank ***/
  t = timing[int(Level::SubBank)];

  // CAS <-> RAS
  t[int(Command::ACT)].push_back({Command::RD, 1, s.nRCDR});
  t[int(Command::ACT)].push_back({Command::RDA, 1, s.nRCDR});
  t[int(Command::ACT)].push_back({Command::PIMRD, 1, s.nRCDR});
  t[int(Command::ACT)].push_back({Command::WR, 1, s.nRCDW});
  t[int(Command::ACT)].push_back({Command::WRA, 1, s.nRCDW});
  t[int(Command::ACT)].push_back({Command::PIMWR, 1, s.nRCDW});

  t[int(Command::RD)].push_back({Command::PRE, 1, s.nRTP});
  t[int(Command::WR)].push_back({Command::PRE, 1, s.nCWL + s.nBL + s.nWR});
  t[int(Command::PIMRD)].push_back({Command::PRE, 1, s.nRTP});
  t[int(Command::PIMWR)].push_back({Command::PRE, 1, s.nCWL + s.nBL + s.nWR});

  t[int(Command::RDA)].push_back({Command::ACT, 1, s.nRTP + s.nRP});
  t[int(Command::WRA)].push_back(
      {Command::ACT, 1, s.nCWL + s.nBL + s.nWR + s.nRP});

  // RAS <-> RAS
  t[int(Command::ACT)].push_back({Command::ACT, 1, s.nRC});
  t[int(Command::ACT)].push_back({Command::PRE, 1, s.nRAS});
  t[int(Command::PRE)].push_back({Command::ACT, 1, s.nRP});

  /*** Bank ***/
  t = timing[int(Level::Bank)];

  // REFSB
  t[int(Command::PRE)].push_back({Command::REFSB, 1, s.nRP});
  t[int(Command::REFSB)].push_back({Command::REFSB, 1, s.nRFC});
  t[int(Command::REFSB)].push_back({Command::ACT, 1, s.nRFC});
}

int PIM::local_bank_bit() { return calc_log2(num_local_bank_); }

int PIM::calc_log2(int val) {
  int n = 0;
  while ((val >>= 1))
    n++;
  return n;
}
