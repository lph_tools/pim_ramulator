#include "Config.h"
#include "DummyMemory.h"
#include "Memory.h"
#include "PimInstruction.h"
#include "PimInstructionGenerator.h"
#include "PimOp.h"
#include "PimPacket.h"
#include "ProcessingElement.h"
#include "common.h"
#include "gtest/gtest.h"

using namespace ramulator;
namespace {

class PeTest : public testing::Test {
protected:
  virtual void SetUp() {
    Config cfg;
    PeConfig pecfg{0, 0, 1024};
    dm_ = new DummyMemory();
    pe_ = new ProcessingElement(cfg, dm_, pecfg);
    pig_ = new PimInstructionGenerator(dm_, pe_);
    packet_.op = 3; // COPY operation
    packet_.base1 = 0;
    packet_.baser = 512;
    packet_.size = 128;
    pe_->launch(packet_);
    pig_->set_operation(packet_);
  }

  virtual void TearDown() {
    // reset pe and pig before each test
    pe_->launch(packet_);
    pig_->set_operation(packet_);
  }

  PimInstructionGenerator *pig_;
  ProcessingElement *pe_;
  DummyMemory *dm_;
  PimPacket packet_;
};

TEST_F(PeTest, pig_with_pop) {
  PimInstruction *p1 = pig_->front();
  pig_->pop();
  PimInstruction *p2 = pig_->front();

  EXPECT_NE(p1, p2);
}

TEST_F(PeTest, pig_without_pop) {
  PimInstruction *p1 = pig_->front();
  PimInstruction *p2 = pig_->front();

  EXPECT_EQ(p1, p2);
}

TEST_F(PeTest, pig_end_of_stream) {

  EXPECT_EQ(pig_->front()->pim_op()->type(), PimOpType::NORMAL);

  for (int32_t i = 0; i < packet_.size; ++i)
    pig_->pop();

  PimInstruction *p = pig_->front();

  EXPECT_EQ(p->pim_op()->type(), PimOpType::DONE);
}

TEST_F(PeTest, pe_done) {
  const int32_t num_pipe_stage = 14;
  const int32_t expected_cycle = packet_.size + num_pipe_stage;
  for (int32_t i = 0; i < expected_cycle; ++i) {
#ifdef DEBUG
    std::cout << "Cycle " << i << ":" << std::endl;
#endif

    pe_->tick();
    dm_->tick();
  }

  EXPECT_TRUE(pe_->done());
}

} // namespace