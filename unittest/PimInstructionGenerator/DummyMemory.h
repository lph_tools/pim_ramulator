#include "Memory.h"

class DummyMemory final : public MemoryBase {
public:
  DummyMemory() : issued_(false) {}
  virtual ~DummyMemory() {}
  virtual double clk_ns() {}
  virtual void tick() {
    if (issued_) {
      req_.callback(req_);
      issued_ = false;
    }
  }
  virtual bool send(Request req) {
    issued_ = true;
    req_ = req;
    return true;
  }
  int pending_requests() { return 0; }
  void finish(void) {}
  long page_allocator(long addr, int coreid) { return 0; }
  void record_core(int coreid) {}
  void set_high_writeq_watermark(const float watermark) {}
  void set_low_writeq_watermark(const float watermark) {}
  // for PIM
  long calc_addr(long base_addr, long offset) { return 0; }
  uint32_t num_total_bank() { return 8; }
  long bank_capacity() { return 1024; }
  long bank_addr(long index) { return 0; }
  uint32_t prefetch_size() { return 0; }
  uint32_t atom_bytes() { return 0; }

private:
  bool issued_;
  Request req_;
};