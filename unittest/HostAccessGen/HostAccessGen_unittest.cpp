#include "HostAccessGen.h"
#include "RandomAccess.h"
#include "StreamAccess.h"
#include "StreamRandomAccess.h"
#include "common.h"
#include "gtest/gtest.h"

using namespace ramulator;
namespace {
TEST(HostAccessGenTest, RandomAccessTest) {
  HostAccessGen host_gen(RandomAccess(10));

  for (unsigned i = 0; i < 10; ++i) {
    EXPECT_FALSE(host_gen.no_more_addr());
    host_gen.next_addr();
  }

  EXPECT_TRUE(host_gen.no_more_addr());
}

TEST(HostAccessGenTest, StreamAccessTest) {
  HostAccessGen host_gen(StreamAccess(0, 100, 4));

  for (unsigned i = 0; i < 26; ++i) {
    EXPECT_FALSE(host_gen.no_more_addr());
    addr_t addr = host_gen.next_addr();
    EXPECT_EQ(addr, i * 4);
  }

  EXPECT_TRUE(host_gen.no_more_addr());
}

TEST(HostAccessGenTest, StreamRandomAccessTest) {
  HostAccessGen host_gen(StreamRandomAccess(10, 10, 4));
  addr_t base = 0;

  for (unsigned i = 0; i < 100; ++i) {
    EXPECT_FALSE(host_gen.no_more_addr());
    addr_t addr = host_gen.next_addr();
    if (i % 10 == 0)
      base = addr;
    EXPECT_EQ(addr, base + (i % 10) * 4);
  }

  EXPECT_TRUE(host_gen.no_more_addr());
}

} // namespace